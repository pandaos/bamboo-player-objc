#
# Be sure to run `pod lib lint BambooPlayer.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'BambooPlayer'
  s.version          = '1.0.0'
  s.summary          = 'The Bamboo media player library for iOS, written in Objective-C.'

  s.description      = 'This is the official media player library for the Panda-OS Bamboo Video Platform. The player acts as an independent player, as well as part of a Bamboo app.'

  s.homepage         = 'https://bitbucket.org/pandaos/bamboo-player-objc'
  s.authors = { 'Roni Cohen' => 'roni@panda-os.com', '' => 'vladimir@panda-os.com' }
  s.source           = { :git => 'https://bitbucket.org/pandaos/bamboo-player-objc.git', :tag => s.version.to_s }
  s.social_media_url = 'https://www.facebook.com/pandaopensource'

  s.ios.deployment_target = '11.0'

  s.source_files = 'BambooPlayer/Classes/**/*'
  s.resources = 'BambooPlayer/Assets/**/*'
  s.ios.vendored_frameworks='BambooPlayer/Frameworks/ProgrammaticAccessLibrary.xcframework'
  s.frameworks = 'UIKit', 'AVFoundation'
  s.dependency 'PVPClient'
  s.dependency 'ASValueTrackingSlider'
  s.dependency 'AsyncImageView'
  s.dependency 'MZTimerLabel'
  s.dependency 'Firebase'
  s.dependency 'Firebase/Core'
  s.dependency 'Firebase/Analytics'
  s.dependency 'GoogleAds-IMA-iOS-SDK', '3.14.5'
  s.dependency "YoutubePlayer-in-WKWebView", "~> 0.3.0"

#  s.dependency 'PlayKit'

  s.xcconfig = {'LD_RUNPATH_SEARCH_PATHS' => '$(inherited) @executable_path/Frameworks','CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES' => 'YES'}
end
