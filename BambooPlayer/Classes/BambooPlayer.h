//
//  BambooPlayer.h
//  Pods
//
//  Created by Oren Kosto,  on 11/28/16.
//
//

#import <Foundation/Foundation.h>
#import "PVPClient.h"
#import "BambooPlayerView.h"
#import "WKYTPlayerView.h"

/**
 * The BambooPlayer class is the main view for the player. Embed this view in your view controller (possible via the storyboard) to use the player.
 */
@interface BambooPlayer : UIView

@property (nonatomic, strong) BambooPlayerView *playerView;
@property (nonatomic, strong) WKYTPlayerView *youtubePlayerView;

-(int)getPlayerOffset;

/**
 *  @name Public Methods
 */

- (void)initializeSubviews;

/**
 * Sets the class that implements the player delegate we wish to fire player events to.
 *
 * @param delegate The required delegate
 */
-(void)setPlayerDelegate:(id<BambooPlayerDelegate>)delegate;


/**
 * Plays an scheduled item with offset.
 *
 * @param offset of the item.
 */
-(void)playEntryWithOffset:(int) offset;

/**
 * Plays an entry with the VOD (default) gallery type.
 *
 * @param entry The entry.
 *
 * @see [BambooPlayer playEntry:withType:]
 */
-(void)playEntry:(PVPEntry *)entry;

/**
 * Plays an entry with a custom type.
 *
 * @param entry The entry.
 * @param type The entry gallery type.
 *
 * @see [BambooPlayer playEntry:]
 */
-(void)playEntry:(PVPEntry *)entry withType:(BambooGalleryType)type;

-(void)playEntryWithId:(NSString *)entryId;

/**
 * Plays a channel.
 *
 * @param channel The channel.
 */
-(void)playChannel:(PVPChannel *)channel;

-(void)playChannelWithId:(NSString *)channelId;

/**
 * Pauses the player. Call this method when the app us moving to the background, when presenting a modal view controller, or any other case where the player will loses focus.
 */
-(void)pause;

-(void)resume;

- (void) seekToTime:(int)time;

/// This funciton will call continue option in playerViewContainer
- (void)showContinuePlayerView;

- (int)currentTime;

/**
 * refreshes the player state. Call this method when the app is returning from the background, back from presenting a modal view controller,
 * or any other state where the player had lost focus.
 */
-(void)checkPlayerState;

/**
 * Checks if the player is currently playing.
 *
 * @return YES or NO, depending on whether the player is currently playing.
 */
-(BOOL)isPlaying;

-(void)setPlayerLogoUrl:(NSString *)playerLogoUrl;

-(void)setVolume:(float)volume;
-(void)setMute:(BOOL)mute;
-(BOOL)isMuted;

-(void)setShouldRotate:(BOOL)shouldRotate;
-(void)updateNativePlayerLayerFrame;

/**
 * Destroys the player and releases it from the memory. Call this method when exiting the containing view controller.
 */
-(void)destroy;

/**
 * Play YouTube video with ID
 */
- (void)playYoutubeItemWithID:(NSString *)videoID;

@end
