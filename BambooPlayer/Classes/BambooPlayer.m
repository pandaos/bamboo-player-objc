//
//  BambooPlayer.m
//  Pods
//
//  Created by Oren Kosto,  on 11/28/16.
//
//

#import "BambooPlayer.h"
#import "PVPPlayerAnalyticsManager.h"
@import ProgrammaticAccessLibrary;

@interface BambooPlayer ()


@end

@implementation BambooPlayer

- ( instancetype ) initWithFrame:( CGRect )frame {
    self = [super initWithFrame:frame];
    [self initializeSubviews];
    return self;
}

- ( instancetype ) initWithCoder:( NSCoder * )aDecoder {
    self = [super initWithCoder:aDecoder];
    [self initializeSubviews];
    return self;
}



- ( void ) initializeSubviews {
    self.playerView = [[[NSBundle bundleForClass:[BambooPlayerView classForCoder]]
                        loadNibNamed:@"BambooPlayerView"
                        owner:self
                        options:nil] firstObject];
    
    self.youtubePlayerView = [[WKYTPlayerView alloc] initWithFrame: self.playerView.bounds];
    self.youtubePlayerView.backgroundColor = [UIColor blackColor];
    self.youtubePlayerView.webView.backgroundColor = [UIColor blackColor];
    self.youtubePlayerView.hidden = YES;
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleYoutubePlayer)];
    [self.youtubePlayerView addGestureRecognizer:recognizer];
    
    [self addSubview:self.youtubePlayerView];
    
    [self setConstraintForView: self.youtubePlayerView];
    
    [self addSubview: self.playerView];
    [self setConstraintForView: self.playerView];
    
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(appWillResignActive:) name: UIApplicationWillResignActiveNotification object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(appDidBecomeActive:) name: UIApplicationDidBecomeActiveNotification object: nil ];
}

- ( void ) playEntry:( PVPEntry * )entry {
    [self playEntry:entry withType:BambooGalleryTypeVOD];
}

- ( void ) playEntry:( PVPEntry * )entry withType:( BambooGalleryType )type {
    if ([entry isYouTubeEntry]) {
        [self playYoutubeItemWithID:[entry youTubeID]];
    } else {
        [self.playerView playEntry:entry withType:type];
        [self.playerView setHidden:NO];
        [self.youtubePlayerView setHidden:YES];
        [self.youtubePlayerView stopVideo];
    }
}

- ( void ) playEntryWithId:( NSString * )entryId {
    [self.playerView playEntryWithId:entryId];
}

- ( void ) playChannel:( PVPChannel * )channel {
    if ([channel currentLiveScheduleItem] != nil && [channel isYoutubeChannel]) {
        [self playYoutubeItemWithID:[[channel currentLiveScheduleItem] youtubeID]];
    } else {
        [[self playerView] cleanAds];
        [self.playerView playChannel:channel];
        [self.playerView setHidden:NO];
        [self.youtubePlayerView setHidden:YES];
        [self.youtubePlayerView stopVideo];
    }
}

- ( void ) playChannelWithId:(NSString *)channelId {
    [self.playerView playChannelWithId:channelId];
}

- ( void ) pause {
    [self.playerView pause];
    [[self youtubePlayerView] pauseVideo];
}

- ( void ) resume {
    [self.playerView resume];
}

- ( void ) seekToTime:( int )time {
    [self.playerView seekToTime:time];
}
/// This function will call for show/hide continue inline popup in player view
- ( void ) showContinuePlayerView {
    [self.playerView enableContinuePopUpView];
}
- ( int ) currentTime {
    return [self.playerView currentTime];
}

- ( void ) checkPlayerState {
    [self.playerView checkPlayerState];
}

- ( BOOL ) isPlaying {
    return [self.playerView isPlaying];
}

- ( void ) setPlayerDelegate:( id<BambooPlayerDelegate, WKYTPlayerViewDelegate> )delegate {
    self.playerView.delegate = delegate;
    self.youtubePlayerView.delegate = delegate;
}

- ( void ) setPlayerLogoUrl:( NSString * )playerLogoUrl {
    [self.playerView setPlayerLogoUrl:playerLogoUrl];
}

/**
 * Plays an scheduled item with offset.
 *
 * @param offset of the item.
 */
- ( void ) playEntryWithOffset:( int )offset {
    [self.playerView playEntryWithOffset:offset];
}

- ( int ) getPlayerOffset {
    return self.playerView.currentOffset;
}

- ( void ) setVolume:( float )volume {
    [self.playerView setVolume:volume];
}

- ( void ) setMute:( BOOL )mute {
    [self.playerView setMute:mute];
}

- ( BOOL ) isMuted {
    return [self.playerView isMuted];
}

-(void)setShouldRotate:(BOOL)shouldRotate {
    // Fix: PJ100 - Commented out so that PIP would trigger when video is playing in minimized view
    [self.playerView setShouldRotate:shouldRotate];
}

-(void)updateNativePlayerLayerFrame {
    [self.playerView updateNativePlayerLayerFrame];
}

- ( void ) destroy {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [[self youtubePlayerView] stopVideo];
    self.youtubePlayerView.delegate = nil;
    [self.youtubePlayerView removeFromSuperview];
    self.youtubePlayerView = nil;
    
    [self.playerView destroy];
    
    self.playerView.delegate = nil;
    self.playerView = nil;
}

#pragma mark - Youtube Setup & Delegate

- (void)playYoutubeItemWithID:(NSString *)videoID {
    [[self playerView] cleanAds];
    [[self playerView] pause];
    [[self playerView] setPlayPauseButtons:NO];
    [[self playerView] setEntry:nil];
    [[self playerView] setChannel:nil];
    [[self playerView] setNode:nil];
    
    [[[self playerView] nativePlayer] replaceCurrentItemWithPlayerItem:nil];
    [self.playerView setHidden:YES];
    [self.youtubePlayerView setHidden:YES];
    [self.youtubePlayerView loadWithVideoId:videoID playerVars:[self playerVars]];
}

- (void)setConstraintForView: (UIView *) view {
    self.youtubePlayerView.translatesAutoresizingMaskIntoConstraints = NO;
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:view
                                                                     attribute:NSLayoutAttributeTop
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self
                                                                     attribute:NSLayoutAttributeTop
                                                                    multiplier:1.0
                                                                      constant:0.0];
    NSLayoutConstraint *leftConstraint = [NSLayoutConstraint constraintWithItem:view
                                                                      attribute:NSLayoutAttributeLeft
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self
                                                                      attribute:NSLayoutAttributeLeft
                                                                     multiplier:1.0
                                                                       constant:0.0];
    NSLayoutConstraint *rightConstraint = [NSLayoutConstraint constraintWithItem:view
                                                                       attribute:NSLayoutAttributeRight
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self
                                                                       attribute:NSLayoutAttributeRight
                                                                      multiplier:1.0
                                                                        constant:0.0];
    NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:view
                                                                        attribute:NSLayoutAttributeBottom
                                                                        relatedBy:NSLayoutRelationEqual
                                                                           toItem:self
                                                                        attribute:NSLayoutAttributeBottom
                                                                       multiplier:1.0
                                                                         constant:0.0];
    NSArray *constraints = @[topConstraint, leftConstraint, rightConstraint, bottomConstraint];
    [NSLayoutConstraint activateConstraints:constraints];
}

- (void)toggleYoutubePlayer {
    [_youtubePlayerView getPlayerState:^(WKYTPlayerState playerState, NSError * _Nullable error) {
        if (playerState == kWKYTPlayerStatePaused) {
            [self->_youtubePlayerView playVideo];
        } else if (playerState == kWKYTPlayerStatePlaying){
            [self->_youtubePlayerView pauseVideo];
        }
    }];
}

- (NSDictionary *)playerVars {
    return @{
        @"controls" : @0,
        @"playsinline" : @1,
        @"autohide" : @1,
        @"showinfo" : @0,
        @"modestbranding" : @1
    };
}

#pragma mark - Notifications

- (void)appWillResignActive:(NSNotification *) note {
    if (self.playerView.isHidden) {
        if (![[PVPConfigHelper sharedInstance] allowBackgroundPlayback]) {
            [self.youtubePlayerView pauseVideo];
        }
    } else {
        [self.playerView appWillResignActive];
    }
}

- (void)appDidBecomeActive:(NSNotification *) note {
    if (self.playerView.isHidden) {
        if (![[PVPConfigHelper sharedInstance] allowBackgroundPlayback]) {
            [self.youtubePlayerView playVideo];
        }
    } else {
        [self.playerView appDidBecomeActive];
    }
}

@end
