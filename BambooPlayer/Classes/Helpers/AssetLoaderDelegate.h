//
//  AssetLoaderDelegate.h
//  Pods
//
//  Created by Oren Kosto,  on 8/9/17.
//
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreMedia/CoreMedia.h>

@interface AssetLoaderDelegate : NSObject <AVAssetResourceLoaderDelegate>

@property(nonatomic, strong) NSString *entryId;

- (instancetype)initWithEntryId:(NSString *)entryId;

@end
