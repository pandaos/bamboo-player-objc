//
//  KalturaAnalyticsHelper.m
//  Pods
//
//  Created by Oren Kosto,  on 11/27/16.
//
//

#import "KalturaAnalyticsHelper.h"

#define MAX_CONNECTIONS 10

@interface KalturaAnalyticsHelper ()

@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSString *baseUrl;
@property (nonatomic, strong) NSMutableDictionary *params;
@property (nonatomic, strong) NSMutableDictionary<NSString *, NSURLSessionDataTask *> *tasksDict;
@property (nonatomic, assign) BOOL isFirstInSession;

@end

@implementation KalturaAnalyticsHelper

static BOOL isSetup = NO;
static KalturaAnalyticsHelper *sharedInstance = nil;
static dispatch_once_t onceToken;

+ (instancetype)sharedInstance {
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    if (!isSetup) {
        [sharedInstance setup];
        isSetup = YES;
    }
    return sharedInstance;
}

-(void) setup {
    self.session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    self.baseUrl = [[PVPConfigHelper sharedInstance] kalturaStatsUrl];
    self.params = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                  @"service": @"stats",
                                                                  @"apiVersion":@"3.1",
                                                                  @"expiry": @"86400",
                                                                  @"clientTag": @"kwidget%3Av2.55.1",// client full version
                                                                  @"format": @"1",
                                                                  @"ignoreNull": @"1",
                                                                  @"action": @"collect",
                                                                  @"event:clientVer": @"2.55.1",// change to client version
                                                                  @"event:objectType": @"KalturaStatsEvent",
                                                                  @"event:partnerId": [[PVPConfigHelper sharedInstance] partnerId],
                                                                  @"event:uiconfId": [[PVPConfigHelper sharedInstance] playerUiConfId],
                                                                  @"event:widgetId": [NSString stringWithFormat:@"_%@", [[PVPConfigHelper sharedInstance] partnerId]],
                                                                  @"event:referrer": @"iOS"
                                                                  }];
    
    if ([PVPConfigHelper sharedInstance].currentConfig.userKs && [PVPConfigHelper sharedInstance].currentConfig.userKs.length > 0) {
        self.params[@"ks"] = [PVPConfigHelper sharedInstance].currentConfig.userKs;
    }
    
    self.isFirstInSession = YES;
}

-(NSURL *) eventUrlWithParams:(NSDictionary *)eventParams {
    NSURLComponents *components = [NSURLComponents componentsWithString:self.baseUrl];
    NSArray *queryItems = @[];
    
    NSDictionary *finalParams = [self.params dictionaryByMergingWithDictionary:eventParams];
    for (NSString *key in finalParams) {
        BOOL isString = [finalParams[key] isKindOfClass:[NSString class]];
        if ( isString ) {
            queryItems = [queryItems arrayByAddingObject: [NSURLQueryItem queryItemWithName:key value:finalParams[key]] ];
        }
    }

    components.queryItems = queryItems;
    
    return components.URL;
}

-(void) sendEventWithType:(KalturaAnalyticsEventType)type withEntry:(PVPEntry *)entry currentPoint:(int)currentPoint {
    NSTimeInterval now = [[NSDate date] timeIntervalSince1970WithServerOffset];
    NSString *entryId = entry && entry.id ? entry.id : @"";
    int duration = entry && entry.duration > 0 ? entry.duration : 0;
    NSURL *url = [self eventUrlWithParams:@{
                                            @"event:currentPoint": [NSString stringWithFormat:@"%d", currentPoint],
                                            @"event:duration": [NSString stringWithFormat:@"%d", duration],
                                            @"event:eventTimestamp": [NSString stringWithFormat:@"%d", (int)now],
                                            @"event:isFirstInSession": self.isFirstInSession ? @"true" : @"false",
                                            @"event:entryId": entryId,
                                            @"event:eventType": [NSString stringWithFormat:@"%d", (int)type],
                                            @"event:seek": type == KalturaAnalyticsEventTypeSeek ? @"true" : @"false"
                                            }];
    NSLog(@"sending stats: %@", url.absoluteString);
    self.isFirstInSession = NO;
    NSURLSessionDataTask *task = [self.session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSLog(@"finished");
    }];
    [task resume];
}


@end
