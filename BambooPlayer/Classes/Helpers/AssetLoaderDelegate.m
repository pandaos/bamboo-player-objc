//
//  AssetLoaderDelegate.m
//  Pods
//
//  Created by Oren Kosto,  on 8/9/17.
//
//

#import "AssetLoaderDelegate.h"
#import <PVPClient/PVPClient.h>

NSString* const URL_SCHEME_NAME = @"skd";
int const KEY_SERVER_OK = 200;
//NSString* const KEY_SERVER_URL = @"https://fps.ezdrm.com/api/licenses/{assetId}?format=fairplay&bambooToken={token}&iid={instanceId}&entryId={entryId}&country={country}&";

@implementation AssetLoaderDelegate

/*
 * Init the delegate object.
 */
- (id)init
{
    self = [super init];
    
    @throw [NSException exceptionWithName:NSInvalidArgumentException
                                   reason:[NSString stringWithFormat:@"Cannot init an instance of %@ without an entry ID. Please use initWithEntryId:", NSStringFromClass([self class])]
                                 userInfo:nil];
    
    return self;
}

- (instancetype)initWithEntryId:(NSString *)entryId {
    self = [super init];
    self.entryId = entryId;
    return self;
}

/* ---------------------------------------------------------
 **
 **  getContentKeyAndLeaseExpiryfromKeyServerModuleWithRequest:
 **
 **  Send the SPC to a Key Server that contains your Key Security
 **  Module (KSM). The KSM decrypts the SPC and gets the requested
 **  CK from the Key Server. The KSM wraps the CK inside an encrypted
 **  Content Key Context (CKC) message, which it sends to the app.
 **
 **  The application may use whatever transport forms and protocols
 **  it needs to send the SPC to the Key Server.
 **
 ------------------------------------------------------- */
- (NSData *)getContentKeyAndLeaseExpiryfromKeyServerModuleWithRequest:(NSData *)requestBytes
                                                      manifestAssetId:(NSString *)assetId
                                                         customParams:(NSDictionary *)query
                                                  leaseExpiryDuration:(NSTimeInterval *)expiryDuration
                                                                error:(NSError **)errorOut
{
    NSData *decodedData = nil;
    NSString *keyServerUrl = [[PVPConfigHelper sharedInstance] drmServiceUrlFairplay];
    for (NSString *key in [query allKeys]) {
        keyServerUrl = [keyServerUrl stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"{%@}", key]
                                                               withString:query[key]];
    }
    NSString *url = [keyServerUrl stringByAppendingString:assetId];
    NSURL *ksmURL = [NSURL URLWithString:url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:ksmURL];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/octet-stream" forHTTPHeaderField:@"Content-type"];
    // Additionally, if the length of your upload body data can be determined automatically (for example, if you provide the body content with an NSData object), then the value of Content-Length is set for you.
    [request setHTTPBody:requestBytes];
    
    NSError *error = nil;
    NSHTTPURLResponse *response = nil;
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request
                                         returningResponse:&response
                                                     error:&error];
    
    if(error == nil) {
        if([response statusCode] == KEY_SERVER_OK) {
            decodedData = data;
            
/*
 If needed to schedule license renewal, a separate parameter could be supplied from the server \
 which contains a value of interval to apply for license renewal in s. It can be stored for future use!
 */
            //*expiryDuration = (double)500;
        }
        else {
            *errorOut = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorBadServerResponse userInfo:nil];
        }
    }
    else {
        *errorOut = [NSError errorWithDomain:NSPOSIXErrorDomain code:-1 userInfo:@{NSUnderlyingErrorKey: error}];
    }
    
    return decodedData;
}


- (NSData *)getAppCertificate: (NSString *)assetId
{
    NSData *certificate = nil;
    
    NSString *certPath = [[PVPConfigHelper sharedInstance] drmFairplayCertificatePath];
    certificate = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:certPath]];
    
    return certificate;
}

/* ---------------------------------------------------------
 **
 **  resourceLoader:shouldWaitForLoadingOfRequestedResource:
 **
 **   When iOS asks the app to provide a CK, the app invokes
 **   the AVAssetResourceLoader delegate’s implementation of
 **   its -resourceLoader:shouldWaitForLoadingOfRequestedResource:
 **   method. This method provides the delegate with an instance
 **   of AVAssetResourceLoadingRequest, which accesses the
 **   underlying NSURLRequest for the requested resource together
 **   with support for responding to the request.
 **
 ** ------------------------------------------------------- */
- (BOOL)resourceLoader:(AVAssetResourceLoader *)resourceLoader shouldWaitForLoadingOfRequestedResource:(AVAssetResourceLoadingRequest *)loadingRequest
{
    NSURL *assetURI = loadingRequest.request.URL;
    
    // URI formated like skd://ddd.host.com/;assetId
    NSString* assetId = [assetURI parameterString];
    NSString* scheme = [assetURI scheme];
    
    if (![scheme isEqual:@"skd"])
        return NO;
    
    NSLog(@"assetId:  %@", assetId);
    
    // need to wrap in data
    NSData *assetIdData = [NSData dataWithBytes:[assetId cStringUsingEncoding:NSUTF8StringEncoding]
                                         length:[assetId lengthOfBytesUsingEncoding:NSUTF8StringEncoding]];
    
    NSData  *certificate = [self getAppCertificate:assetId];
    if(certificate == nil) {
        [loadingRequest finishLoadingWithError:[NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorClientCertificateRequired userInfo:nil]];
        
        return YES;
    }
    
    NSData  *requestBytes = nil;
    NSError *topLevelError = nil;
    
    //Additional information necessary to obtain the key, or nil if no additional information is required. (streamingContentKeyRequestDataForApp::options)
    
    // to obtain the SPC message from iOS to send to the Key Server.
    requestBytes = [loadingRequest streamingContentKeyRequestDataForApp:certificate
                                                      contentIdentifier:assetIdData
                                                                options:nil
                                                                  error:&topLevelError];
    
    if(requestBytes == nil && topLevelError != nil) {
        NSError *underlyingError = [[topLevelError userInfo] objectForKey:NSUnderlyingErrorKey];
        if([[underlyingError domain] isEqualToString:NSOSStatusErrorDomain]) {
            NSInteger errorCode = [underlyingError code];
            NSLog(@"ERROR:  %li", (long)errorCode);
        }
        
        // Causes the receiver to treat the request as having failed.
        [loadingRequest finishLoadingWithError:topLevelError];
        
        return YES;
    }
    
    NSString *token = [[AppUtils getDefaultValueFofKey:USER_TOKEN_KEY] stringByReplacingOccurrencesOfString:@"token=" withString:@""];
    NSString *instanceId = [[PVPConfigHelper sharedInstance] currentInstanceId];
    NSString *country = [PVPConfigHelper getLocalConfiguration:@"X-Bamboo-Country"];
    NSDictionary *passthruParams = @{
                                         @"format": @"fairplay",
                                         @"token" : token ? token : @"",
                                         @"instanceId": instanceId ? instanceId : @"",
                                         @"country": country ? country : @"",
                                         @"entryId": self.entryId ? self.entryId : @"",
                                         @"assetId": assetId ? assetId : @""
                                         };
    NSData *responseData = nil;
    NSTimeInterval expiryDuration = 0.0;
    NSError *error = nil;
    
    // Send the SPC message to the Key Server.
    responseData = [self getContentKeyAndLeaseExpiryfromKeyServerModuleWithRequest:requestBytes
                                                                   manifestAssetId:@"" //assetId
                                                                      customParams:passthruParams
                                                               leaseExpiryDuration:&expiryDuration
                                                                             error:&error];
    
    // The Key Server returns the CK inside an encrypted Content Key Context (CKC) message in response to
    // the app’s SPC message.  This CKC message, containing the CK, was constructed from the SPC by a
    // Key Security Module in the Key Server’s software.
    if (responseData != nil) {
        
        // An instance of AVAssetResourceLoadingDataRequest that indicates the range of resource data that's being requested. The value of this property will be nil if no data is being requested.
        AVAssetResourceLoadingDataRequest *dataRequest = loadingRequest.dataRequest;
        
        // Provide the CKC message (containing the CK) to the loading request.
        [dataRequest respondWithData:responseData];
        
        // Get the CK expiration time from the CKC. This is used to enforce the expiration of the CK.
        if (expiryDuration != 0.0) {
            
            AVAssetResourceLoadingContentInformationRequest *infoRequest = loadingRequest.contentInformationRequest;
            
            //  Whenever the value is not nil, the request includes a query for the information that AVAssetResourceLoadingContentInformationRequest encapsulates.
            // In response to such queries, the resource loading delegate should set the values of the content information request's properties appropriately before invoking the AVAssetResourceLoadingRequest method finishLoading.
            if (infoRequest) {
                
                // Set the date at which a renewal should be triggered.
                // Before you finish loading an AVAssetResourceLoadingRequest, if the resource
                // is prone to expiry you should set the value of this property to the date at
                // which a renewal should be triggered. This value should be set sufficiently
                // early enough to allow an AVAssetResourceRenewalRequest, delivered to your
                // delegate via -resourceLoader:shouldWaitForRenewalOfRequestedResource:, to
                // finish before the actual expiry time. Otherwise media playback may fail.
                NSTimeInterval safeMargin = 10.0;
                infoRequest.renewalDate = [NSDate dateWithTimeIntervalSinceNow:(expiryDuration - safeMargin)];
                
                infoRequest.contentType = @"application/octet-stream";
                infoRequest.contentLength = responseData.length;
                infoRequest.byteRangeAccessSupported = NO;
            }
        }
        
        // Treat the processing of the request as complete.
        [loadingRequest finishLoading];
    }
    else {
        /*
         When a resource loader’s delegate takes responsibility for loading a resource, it calls this method when a failure occurred when loading the resource.
         This method marks the loading request as finished and notifies the resource loader object that the resource could not be loaded.
         */
        
        // Causes the receiver to treat the request as having failed.
        [loadingRequest finishLoadingWithError:error];
    }
    
    return YES;
}


/* -----------------------------------------------------------------------------
 **
 ** resourceLoader: shouldWaitForRenewalOfRequestedResource:
 **
 ** Delegates receive this message when assistance is required of the application
 ** to renew a resource previously loaded by
 ** resourceLoader:shouldWaitForLoadingOfRequestedResource:. For example, this
 ** method is invoked to renew decryption keys that require renewal, as indicated
 ** in a response to a prior invocation of
 ** resourceLoader:shouldWaitForLoadingOfRequestedResource:. If the result is
 ** YES, the resource loader expects invocation, either subsequently or
 ** immediately, of either -[AVAssetResourceRenewalRequest finishLoading] or
 ** -[AVAssetResourceRenewalRequest finishLoadingWithError:]. If you intend to
 ** finish loading the resource after your handling of this message returns, you
 ** must retain the instance of AVAssetResourceRenewalRequest until after loading
 ** is finished. If the result is NO, the resource loader treats the loading of
 ** the resource as having failed. Note that if the delegate's implementation of
 ** -resourceLoader:shouldWaitForRenewalOfRequestedResource: returns YES without
 ** finishing the loading request immediately, it may be invoked again with
 ** another loading request before the prior request is finished; therefore in
 ** such cases the delegate should be prepared to manage multiple loading
 ** requests.
 **
 ** -------------------------------------------------------------------------- */
- (BOOL)resourceLoader:(AVAssetResourceLoader *)resourceLoader shouldWaitForRenewalOfRequestedResource:(AVAssetResourceRenewalRequest *)renewalRequest
{
    NSLog(@"shouldWaitForRenewalOfRequestedResource");
    return [self resourceLoader:resourceLoader shouldWaitForLoadingOfRequestedResource:renewalRequest];
}

@end
