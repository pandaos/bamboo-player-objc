//
//  KalturaAnalyticsHelper.h
//  Pods
//
//  Created by Oren Kosto,  on 11/27/16.
//
//

#import <Foundation/Foundation.h>
#import "PVPConfigHelper.h"
#import "PVPEntry.h"
#import "NSDate+PVPClient.h"

/**
 * The Kaltura analytics event type enum.
 */
typedef enum : NSUInteger {

    /**
     * Loaded widget event.
     */
    KalturaAnalyticsEventTypeWidgetLoaded = 1,

    /**
     * Loaded media event.
     */
    KalturaAnalyticsEventTypeMediaLoaded = 2,

    /**
     * Play event.
     */
    KalturaAnalyticsEventTypePlay = 3,

    /**
     * Reached 25% event.
     */
    KalturaAnalyticsEventTypePlayReached25 = 4,

    /**
     * Reached 50% event.
     */
    KalturaAnalyticsEventTypePlayReached50 = 5,

    /**
     * Reached 75% event.
     */
    KalturaAnalyticsEventTypePlayReached75 = 6,

    /**
     * Reached 100% event.
     */
    KalturaAnalyticsEventTypePlayReached100 = 7,

    /**
     * Open edit event.
     */
    KalturaAnalyticsEventTypeOpenEdit = 8,

    /**
     * Open viral event.
     */
    KalturaAnalyticsEventTypeOpenViral = 9,

    /**
     * Open download event.
     */
    KalturaAnalyticsEventTypeOpenDownload = 10,

    /**
     * Open report event.
     */
    KalturaAnalyticsEventTypeOpenReport = 11,

    /**
     * Buffer start event.
     */
    KalturaAnalyticsEventTypeBufferStart = 12,

    /**
     * Buffer end event.
     */
    KalturaAnalyticsEventTypeBufferEnd = 13,

    /**
     * Open full screen event.
     */
    KalturaAnalyticsEventTypeOpenFullScreen = 14,

    /**
     * Close full screen event.
     */
    KalturaAnalyticsEventTypeCloseFullScreen = 15,

    /**
     * Replay event.
     */
    KalturaAnalyticsEventTypeReplay = 16,

    /**
     * Seek event.
     */
    KalturaAnalyticsEventTypeSeek = 17,

    /**
     * Open upload event.
     */
    KalturaAnalyticsEventTypeOpenUpload = 18,

    /**
     * Save publish event.
     */
    KalturaAnalyticsEventTypeSavePublish = 19,

    /**
     * Close editor event.
     */
    KalturaAnalyticsEventTypeCloseEditor = 20,

    /**
     * Pre-bumper played event.
     */
    KalturaAnalyticsEventTypePreBumperPlayed = 21,

    /**
     * Post-bumper played event.
     */
    KalturaAnalyticsEventTypePostBumperPlayed = 22,

    /**
     * Bumper clicked event.
     */
    KalturaAnalyticsEventTypeBumperClicked = 23,

    /**
     * Unused.
     */
    KalturaAnalyticsEventTypeFUTURE_USE_1 = 24,

    /**
     * Unused.
     */
    KalturaAnalyticsEventTypeFUTURE_USE_2 = 25,

    /**
     * Unused.
     */
    KalturaAnalyticsEventTypeFUTURE_USE_3 = 26
} KalturaAnalyticsEventType;

/**
 * The KalturaAnalyticsHelper fires events to the Kaltura analytics system. This class is managed by the BamboPlayer library and should not be accessed directly.
 */
@interface KalturaAnalyticsHelper : NSObject

/**
 *  @name Public Methods
 */

/**
 *  Get singleton of the KalturaAnalyticsHelper
 *
 *  @return KalturaAnalyticsHelper singleton instance
 */
+ (nonnull instancetype)sharedInstance;


/**
 Sends a stats event from the player.

 @param type The event type.
 @param entry The currently playing entry.
 @param currentPoint The current playing point of the player.

 */
-(void) sendEventWithType:(KalturaAnalyticsEventType)type withEntry:(PVPEntry * _Nonnull)entry currentPoint:(int)currentPoint;

@end
