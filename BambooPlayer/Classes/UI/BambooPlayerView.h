//
//  BambooPlayerView.h
//  Pods
//
//  Created by Oren Kosto,  on 11/28/16.
//
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import <MediaPlayer/MediaPlayer.h>

#import <MBProgressHUD/MBProgressHUD.h>
#import <SDWebImage/UIImage+GIF.h>

#import <ASValueTrackingSlider/ASValueTrackingSlider.h>
#import <AsyncImageView/AsyncImageView.h>
#import <Firebase/Firebase.h>
#import <FirebaseCore/FirebaseCore.h>
//#import <GVRSDK/GVRVideoView.h>
//#import <PlayKit/PlayKit-Swift.h>
#import "PVPClient.h"
#import "KalturaAnalyticsHelper.h"


/**
 * The player state enum.
 */
typedef enum : NSUInteger {

    /**
     * Unknown state.
     */
    BambooPlayerStateUnknown,

    /**
     * Ready state.
     */
    BambooPlayerStateReady,

    /**
     * Playing state.
     */
    BambooPlayerStatePlaying,

    /**
     * Paused state.
     */
    BambooPlayerStatePaused,
//    BambooPlayerStateEnded,
//    BambooPlayerStateBuffering,

    /**
     * Seeking state.
     */
    BambooPlayerStateSeeking,
    BambooPlayerStateWillNotPlay
} BambooPlayerState;

/**
 * The BambooPlayerDelegate provides an interface for responding to changes in the player state.
 */
@protocol BambooPlayerDelegate <NSObject>
@optional

/**
 * Fired when there was a change in the player state.
 * @param newState The new player state.
 */
-(void) bambooPlayerStateDidChange:(BambooPlayerState)newState;

-(void) bambooPlayerVideoRectDidChange:(CGRect)newRect;

-(void) bambooPlayerDidChangeOffset:(NSInteger)offset;

@end

/**
 * The BambooPlayerView is the player main view. This view should be managed by the BambooPlayer class and shouldn't be accessed directly.
 */
@interface BambooPlayerView : UIView <ASValueTrackingSliderDataSource, PVPUserModelDelegate, PVPConfigModelDelegate> {
    BOOL sent25Percent;
    BOOL sent50Percent;
    BOOL sent75Percent;
    BOOL sent100Percent;

    MBProgressHUD *progressDialog;
}

/**
 * @name Public Properties
 */
/**
 * The video view.
 */
@property (weak, nonatomic) IBOutlet UIView *videoView;

/**
 * The player overlay, containing the large pause/play button, the entry/channel thumbnail, etc.
 */
@property (weak, nonatomic) IBOutlet UIView *playerOverlayView;

/**
 * The player controls container view.
 */
@property (weak, nonatomic) IBOutlet UIView *playerControlsView;

/**
 * The Live player controls container view.
 */
@property (weak, nonatomic) IBOutlet UIView *livePlayerControlsView;


/**
 * Kaltura playkit player
 */
//@property (nonatomic, strong) id<Player> kPlayer;

/**
 * The large play/pause button.
 */
@property (weak, nonatomic) IBOutlet UIButton *playPauseToggleButtonLarge;

/**
 * The small play/pause button.
 */
@property (weak, nonatomic) IBOutlet UIButton *playPauseToggleButton;

/**
 * The 15 Second  forward/backward button on player
 */
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *backwardToggleButton;// outlet for backward 15 second
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *forwardToggleButton; // outlet for forward 15 second
@property (unsafe_unretained, nonatomic) IBOutlet UIView *playerPausePlayContainerView; // this view hold play, forward/backward 15 second button view
@property (unsafe_unretained, nonatomic) IBOutlet UIView *playerControlsBackgroundView; // this view is for background for video controls like label, slider and image
/**
 * This view is for continue watch
 */
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *captionButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *continueInlinePopUp;

/**
 * The small play/pause button on live controller
 */
@property (weak, nonatomic) IBOutlet UIButton *livePlayPauseToggleButton;


/**
 * The player logo.
 */
@property (unsafe_unretained, nonatomic) IBOutlet AsyncImageView *playerLogo;

@property (unsafe_unretained, nonatomic) IBOutlet AsyncImageView *livePlayerLogo;

/**
 * The player logo width constraint.
 */
@property (unsafe_unretained, nonatomic) IBOutlet NSLayoutConstraint *playerLogoWidthConstraint;

/**
 * The replay button.
 */
@property (weak, nonatomic) IBOutlet UIButton *forwardButton;

/**
 * The replay button.
 */
@property (weak, nonatomic) IBOutlet UIButton *rewindButton;

/**
 * The label indicating the elapsed time.
 */
@property (weak, nonatomic) IBOutlet UILabel *timeElapsedLabel;

/**
 * The label indicating the elapsed time.
 */
@property (weak, nonatomic) IBOutlet UILabel *catchUpTimeLabel;


/**
 * The label indicating the remaining time.
 */
@property (weak, nonatomic) IBOutlet UILabel *timeLeftLabel;

/**
 * The player seek slider.
 */
@property (weak, nonatomic) IBOutlet ASValueTrackingSlider *playbackSeekSlider;

/**
 * The entry/channel thumbnail image view.
 */
@property (weak, nonatomic) IBOutlet AsyncImageView *thumbImage;

/**
 * The live indicator container view.
 */
@property (weak, nonatomic) IBOutlet UIView *liveIndicatorContainerView;

/**
 * The live indicator container label.
 */
@property (weak, nonatomic) IBOutlet UILabel *liveIndicatorLabel;

/**
 * The live indicator container icon.
 */
@property (weak, nonatomic) IBOutlet UIImageView *liveIndicatorIcon;

@property (unsafe_unretained, nonatomic) IBOutlet UIView *playerCountdownContainer;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *playerCountdownTimer;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *playerCountdownTitle;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *playerCountdownDaysLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *playerCountdownHoursLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *playerCountdownMinutesLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *playerCountdownSecondsLabel;

@property (unsafe_unretained, nonatomic) IBOutlet UIView *pipMessageView;


@property (unsafe_unretained, nonatomic) IBOutlet UIButton *playerWindowArrangerButton;
- (IBAction)seekSliderValueChanged:(ASValueTrackingSlider *)sender;

/**
 * The delegate class that responds to player events.
 */
@property (weak, nonatomic) id<BambooPlayerDelegate> delegate;

/**
 * The current player state.
 */
@property (assign, nonatomic, readonly) BambooPlayerState state;

/**
 * The currently playing entry. Will be set to nil when playing a channel.
 */
@property (nonatomic, strong) PVPEntry *entry;

/**
 * The currently playing channel. Will be set to nil when playing an entry.
 */
@property (nonatomic, strong) PVPChannel *channel;

/**
 * The currently playing channel. Will be set to nil when playing an entry.
 */
@property (nonatomic, strong) PVPNode *node;

/**
 * The entry/gallery type.
 */
@property (assign, nonatomic) BambooGalleryType galleryType;

/**
 * The player.
 */
@property(nonatomic, strong) AVPlayer *nativePlayer;

/**
 * An optional pre-loaded thumbnail image for the entry.
 *
 */
@property (strong, nonatomic) UIImage *preloadedImage;

@property (assign, nonatomic, readonly) CGRect videoRect;

@property (nonatomic, strong) PVPConfigModel *configModel;
@property(assign, nonatomic) BOOL controlsRevealed;

@property(nonatomic) BOOL shouldRotate;

@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *watermarkImageView;

-(void)setupColors;

//-(void)setupSupportPiP;
- (void)setControlsRevealed:(BOOL)revealed;
- (void)setPlayerAssetsHidden;
- (void)setPlayPauseButtons:(BOOL)isPlay;
-(void)setVolume:(float)volume;
-(void)setMute:(BOOL)mute;
-(BOOL)isMuted;

/**
 * @name Public methods
 */


/**
 * Plays an scheduled item with offset.
 *
 * @param offset of the item.
 */
-(void)playEntryWithOffset:(int) offset;

/**
 * Plays an entry.
 *
 * @param entry The entry.
 * @param type The entry/gallery type.
 */
-(void)playEntry:(PVPEntry *)entry withType:(BambooGalleryType)type;

-(void)playEntryWithId:(NSString *)entryId;

/**
 * Plays a channel.
 * @param channel The channel.
 */
-(void)playChannel:(PVPChannel *)channel;

-(void)playChannelWithId:(NSString *)channelId;

/**
 * Pauses the player.
 */
- (void)pause;
- (void)cleanAds;
/**
 *
 */
- (void)resume;

- (void) seekToTime:(int)time;

/// This function will show/hide continue popup over player view
-(void) enableContinuePopUpView;

-(int)currentTime;

/**
 * Refreshes the player state.
 */
- (void)checkPlayerState;

/**
 * Checks if the player is currently playing.
 *
 * @return YES or NO, depending on whether the player is currently playing.
 */
- (BOOL)isPlaying;

-(void)setPlayerLogoUrl:(NSString *)playerLogoUrl;

-(void)sendGoogleAnalyticsEventWithAction:(NSString *)action withCurrentTime:(int)currentTime;

/**
 * Destroys the player and releases it from the memory.
 */
-(void)destroy;

- (void)appWillResignActive;
- (void)appDidBecomeActive;

- (void)updateNativePlayerLayerFrame;

@property (nonatomic, assign) int currentOffset;
@property (nonatomic, assign) int lastOffsetCreationTime;
@property (nonatomic, strong) NSString *hlsDvrUrl;
@property (nonatomic, strong) NSString *hlsDvrBaseUrl;
@property (nonatomic, strong) NSString *hlsAbrBase;
@property (nonatomic, strong) NSString *hlsAbrStreamNameSuffix;
@property (nonatomic, strong) NSString *hlsDvrStreamNameSuffix;
@property (nonatomic, assign) BOOL dvrIsPlay;
@property (nonatomic, assign) BOOL showContinuePopUp;

@end
