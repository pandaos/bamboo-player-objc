//
//  BambooPlayerView.m
//  Pods
//
//  Created by Oren Kosto,  on 11/28/16.
//
//
#define UIViewParentController(__view) ({ \
    UIResponder *__responder = __view; \
    while ([__responder isKindOfClass:[UIView class]]) \
        __responder = [__responder nextResponder]; \
    (UIViewController *)__responder; \
})
#import "BambooPlayerView.h"
#import "AssetLoaderDelegate.h"
#import "UIView+PVPClient.h"
#import <AdSupport/ASIdentifierManager.h>
@import GoogleInteractiveMediaAds;
#import "MZTimerLabel.h"
#include <stdlib.h>
#import <CommonCrypto/CommonCrypto.h>
#import "BambooActivityAnimationView.h"
#import "PVPPlayerAnalyticsManager.h"
#import <QuartzCore/QuartzCore.h>
#import "NSString+MD5.h"
#import "SDWebImage.h"
@import ProgrammaticAccessLibrary;

@interface BambooPlayerView ()<PVPChannelModelDelegate, PVPEntryModelDelegate, PVPLiveEntryModelDelegate, IMAAdsLoaderDelegate, IMAAdsManagerDelegate, MZTimerLabelDelegate, AVPictureInPictureControllerDelegate, PALNonceLoaderDelegate>

@property(nonatomic, strong) AVPlayerLayer *nativePlayerLayer;
@property(nonatomic, strong) AVPlayerItem *nativePlayerItem;

// MARK: PIP properties
@property(nonatomic, strong) AVPictureInPictureController *pipController;
@property(nonatomic, strong) IMAPictureInPictureProxy *pictureInPictureProxy;
@property (strong, nonatomic) NSTimer *pipMessageTimer;

@property(nonatomic, strong) IMAAVPlayerVideoDisplay *videoDisplay;
@property(nonatomic, strong) IMAAdDisplayContainer *adDisplayContainer;

@property(nonatomic, strong) NSString *playUrl;
@property(nonatomic, strong) NSString *entryId;
@property(nonatomic, strong) NSString *playerLogoUrl;
@property(nonatomic, strong) NSTimer *controlsRevealTimer;
@property(assign, nonatomic) BOOL sliderBeingDragged;
@property(assign, nonatomic) BOOL useNativePlayer;
@property(assign, nonatomic) BOOL adIsFinished;

@property(assign, nonatomic) BOOL shouldShowChannelAds;

@property (nonatomic, strong) PVPUserModel *userModel;
@property (nonatomic, strong) PVPEntryModel *entryModel;
@property (nonatomic, strong) PVPLiveEntryModel *liveEntryModel;
@property (nonatomic, strong) PVPChannelModel *channelModel;
@property (nonatomic, strong) NSTimer *updateWatchingTimer;
@property (nonatomic, strong) NSTimer *isLiveTimer;

@property (nonatomic, strong) MZTimerLabel *liveCountdownTimer;
@property (nonatomic, assign) id playerObserverUpdateVideoProgressStats;
@property (nonatomic, assign) id playerObserverUpdateProgressBar;

@property (nonatomic, strong) void (^timeObserverBlockUpdateVideoProgressStats)(CMTime time);
@property (nonatomic, strong) void (^timeObserverBlockUpdateProgressBar)(CMTime time);

/// Entry point for the SDK. Used to make ad requests.
@property (nonatomic, strong) IMAAdsLoader *adsLoader;
/// Playhead used by the SDK to track content video progress and insert mid-rolls.
@property (nonatomic, strong) IMAAVPlayerContentPlayhead *contentPlayhead;
/// Main point of interaction with the SDK. Created by the SDK as the result of an ad request.
@property (nonatomic, strong) IMAAdsManager *adsManager;

@property (strong, nonatomic) AssetLoaderDelegate *loaderDelegate;

@property (assign, nonatomic) float volume;

/** The nonce loader to use for nonce requests. */
@property(nonatomic) PALNonceLoader *nonceLoader;

/** The nonce manager result from the last successful nonce request. */
@property(nonatomic) PALNonceManager *nonceManager;

@end

@implementation BambooPlayerView

static void *AVPlayerStatusObservationContext = &AVPlayerStatusObservationContext;
/* AVAsset keys */
NSString* const BAMBOO_PLAYER_PLAYABLE_KEY = @"playable";
/* AVPlayerItem keys */
NSString* const BAMBOO_PLAYER_STATUS_KEY   = @"status";

NSTimeInterval PLAYER_CONTROLS_FADE_OUT_SECONDS = 2;

// MARK: PIP Timer
double pipMessageTimerInterval = 5.0f;
int playerLogoOriginalWidth = 34;

- ( instancetype ) initWithFrame: ( CGRect ) frame {
    self = [super initWithFrame:frame];
    if ( !self ) {
        return nil;
    }
    return self;
}

- ( instancetype ) initWithCoder: ( NSCoder * ) aDecoder {
    self = [super initWithCoder:aDecoder];
    if ( !self ) {
        return nil;
    }
    return self;
}

- ( void ) willMoveToSuperview: ( UIView * ) newSuperview {
    self.frame = newSuperview.bounds;
    _videoRect = self.frame;
}

- (void) setupNonce {
    self.nonceLoader = [[PALNonceLoader alloc] init];
    self.nonceLoader.delegate = self;
    [self requestNonceManager];
    
}

- ( void ) awakeFromNib {
    [super awakeFromNib];
    
    /// Play sound in silent mode
    UInt32 sessionCategory = kAudioSessionCategory_MediaPlayback;
    AudioSessionSetProperty (kAudioSessionProperty_AudioCategory,
                             sizeof(sessionCategory), &sessionCategory);
    
    self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self setShouldRotate: YES];
    [ self setVolume: 1.0f ];
    [self setupSeekSlider];
    
    if ( !self.userModel ) {
        self.userModel = [ [PVPUserModel alloc] init ];
        self.userModel.delegate = self;
    }
    if ( !self.configModel ) {
        self.configModel = [ [PVPConfigModel alloc] init ];
        self.configModel.delegate = self;
    }
    if ( !self.entryModel ) {
        self.entryModel = [ [PVPEntryModel alloc] init ];
        self.entryModel.delegate = self;
    }
    if ( !self.liveEntryModel ) {
        self.liveEntryModel = [ [PVPLiveEntryModel alloc] init ];
        self.liveEntryModel.delegate = self;
    }
    if ( !self.channelModel ) {
        self.channelModel = [ [PVPChannelModel alloc] init ];
        self.channelModel.delegate = self;
    }
    
    _adIsFinished = YES;
    _shouldShowChannelAds = NO;
    
    self.playerCountdownContainer.hidden = YES;
    [self setupTexts];
    
    // MARK: PIP
    // By Default PIP View will be hidden
    [self.pipMessageView setHidden:YES];
    
    [ [NSNotificationCenter defaultCenter]
      addObserver: self
      selector: @selector( isLoadedFromVOD ) name: @"FROM_VOD" object: nil ];
    
    [ [NSNotificationCenter defaultCenter]
      addObserver: self
      selector: @selector( playerMinimized ) name: @"playerMinimized" object: nil ];
    
    [ [NSNotificationCenter defaultCenter]
      addObserver: self
      selector: @selector( playerMaximized ) name: @"playerMaximized" object: nil ];
}

- (void) playerMinimized {
    self.watermarkImageView.hidden = YES;
}

- (void) playerMaximized {
    self.watermarkImageView.hidden = NO;
}

- (void) isLoadedFromVOD {
    NSString *key = @"PIP_MESSAGE_SEEN";

    if ([[NSUserDefaults standardUserDefaults] boolForKey:key] == false) {
        [[NSUserDefaults standardUserDefaults] setBool:true forKey: key];
       
        if ( [[PVPConfigHelper sharedInstance] googleMobileAdsEnabledAndSet] &&
            [[PVPConfigHelper sharedInstance]allowBackgroundPlayback]) {
            [self.pipMessageView setHidden:NO];
            [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
        } else {
            [self.pipMessageView setHidden:YES];
        }
    }
}

// MARK: PIP Message timer
- (NSTimer *) timer {
    if (!_pipMessageTimer) {
        _pipMessageTimer = [NSTimer timerWithTimeInterval:pipMessageTimerInterval
                                                   target:self selector:@selector(onTick:)
                                                 userInfo:nil
                                                  repeats:NO];
    }
    return _pipMessageTimer;
}

-(void) onTick:(NSTimer*)timer
{
    [self.pipMessageView setHidden:YES];
}

- ( void ) linkPip:(AVPlayerLayer *)playerLayer {
    if (![[PVPConfigHelper sharedInstance] allowBackgroundPlayback]) {
        [self pause:self.galleryType != BambooGalleryTypeChannels && self.galleryType != BambooGalleryTypeLive];
    } else {
        if (AVPictureInPictureController.isPictureInPictureSupported) {
            self.pipController = [[AVPictureInPictureController alloc] initWithPlayerLayer:playerLayer];
            if (@available(iOS 14.2, *)) {
                self.pipController.canStartPictureInPictureAutomaticallyFromInline = true;
            }
            
            // MARK: PIP Use this to enable PIP for ads (Not working now
             // self.pictureInPictureProxy = [[IMAPictureInPictureProxy alloc] initWithAVPictureInPictureControllerDelegate:self];
             //  self.pipController.delegate = self.pictureInPictureProxy;
            
            // MARK: PIP Use this to disable PIP for ads
             self.pipController.delegate = self;
            
        } else {
            NSLog(@"PIP Not supported");
        }
    }
}

- ( void ) setPreloadedImage: ( UIImage * ) preloadedImage {
    _preloadedImage = preloadedImage;
    [self.thumbImage setImage:preloadedImage];
}

- ( void ) setupColors {
    
    self.videoView.backgroundColor = [UIColor blackColor];
    
    //Set player controls background color
    self.playerControlsBackgroundView.backgroundColor = [UIColor colorWithGradientStyle:UIGradientColorStyleTopToBottom withFrame:self.playerControlsBackgroundView.bounds withColors:@[[UIColor clearColor], [[[PVPConfigHelper sharedInstance] backgroundColor] colorWithAlphaComponent:0.5]] withLocations:@[@(0), @(0.8)]];
    // set player play pause controls background color
    //  [self.playerPausePlayContainerView.layer setCornerRadius: 5];
    [self.playerPausePlayContainerView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.playerPausePlayContainerView.layer setShadowOpacity:0.8];
    [self.playerPausePlayContainerView.layer setShadowRadius:10.0];
    [self.playerPausePlayContainerView.layer setShadowOffset:CGSizeMake(5, 5)];
    
    //    Set Live player controlls
    self.livePlayerControlsView.backgroundColor = [UIColor colorWithGradientStyle:UIGradientColorStyleTopToBottom withFrame:self.livePlayerControlsView.bounds withColors:@[[UIColor clearColor], [[[PVPConfigHelper sharedInstance] backgroundColor] colorWithAlphaComponent:0.5]] withLocations:@[@(0), @(0.8)]];
    
    [ self.playPauseToggleButtonLarge setImage: [ UIImage imageNamed: @"playIcon" ] forState: UIControlStateSelected ];
    [ self.livePlayPauseToggleButton setImage: [ UIImage imageNamed: @"playIcon" ] forState: UIControlStateSelected ];
    [ self.playPauseToggleButtonLarge setImage: [ UIImage imageNamed: @"playIcon" ] forState: UIControlStateSelected ];
    
    [ self.playPauseToggleButtonLarge setImage: [ UIImage imageNamed: @"pauseIcon" ] forState: UIControlStateNormal ];
    [ self.livePlayPauseToggleButton setImage: [ UIImage imageNamed: @"pauseIcon" ] forState: UIControlStateNormal ];
    [ self.playPauseToggleButtonLarge setImage: [ UIImage imageNamed: @"pauseIcon" ] forState: UIControlStateNormal ];
    [ self.forwardToggleButton setImage: [ UIImage imageNamed: @"forwardIcon" ] forState: UIControlStateNormal ];
    [ self.backwardToggleButton setImage: [ UIImage imageNamed: @"backwardIcon" ] forState: UIControlStateNormal ];
    
    self.backwardToggleButton.imageView.contentMode = UIViewContentModeScaleToFill;
    self.playPauseToggleButton.imageView.contentMode = UIViewContentModeScaleToFill;
    self.forwardToggleButton.imageView.contentMode = UIViewContentModeScaleToFill;
    
    self.forwardToggleButton.tintColor = [[PVPConfigHelper sharedInstance] primaryColor];
    self.backwardToggleButton.tintColor = [[PVPConfigHelper sharedInstance] primaryColor];
    self.playPauseToggleButton.tintColor = [[PVPConfigHelper sharedInstance] primaryColor];
    self.livePlayPauseToggleButton.tintColor = [[PVPConfigHelper sharedInstance] primaryColor];
    self.rewindButton.tintColor = [[PVPConfigHelper sharedInstance] primaryColor];
    self.forwardButton.tintColor = [[PVPConfigHelper sharedInstance] primaryColor];
    
    self.playPauseToggleButtonLarge.tintColor = [[PVPConfigHelper sharedInstance] primaryColor];
    self.playerWindowArrangerButton.tintColor =  [[PVPConfigHelper sharedInstance] primaryColor];
    self.captionButton.tintColor =  [[PVPConfigHelper sharedInstance] primaryColor];
    self.timeElapsedLabel.textColor = [[PVPConfigHelper sharedInstance] primaryHighlightColor];
    self.timeLeftLabel.textColor = [[PVPConfigHelper sharedInstance] primaryHighlightColor];
    
    self.liveIndicatorLabel.textColor = [[PVPConfigHelper sharedInstance] primaryTextColor];
    self.liveIndicatorIcon.tintColor = [UIColor redColor];
    
    //seek slider
    self.playbackSeekSlider.minimumTrackTintColor = [[PVPConfigHelper sharedInstance] primaryColor];
    self.playbackSeekSlider.maximumTrackTintColor = [[PVPConfigHelper sharedInstance] secondaryColor];
    [self.playbackSeekSlider setThumbImage:[UIImage imageNamed:@"iconSlider"] forState:UIControlStateNormal];
    [self.playbackSeekSlider setThumbImage:[UIImage imageNamed:@"iconSlider"] forState:UIControlStateHighlighted];
    self.playbackSeekSlider.tintColor = [[PVPConfigHelper sharedInstance] primaryColor];
    self.playbackSeekSlider.popUpViewColor = [[PVPConfigHelper sharedInstance] primaryHighlightColor];
    self.playbackSeekSlider.textColor = [[PVPConfigHelper sharedInstance] playPageSecondaryTextColor];
    self.playbackSeekSlider.font = [UIFont fontWithName:@"Avenir" size:16.0f];
    
    //TODO check RTL / LTR issues
    [self.playbackSeekSlider setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
}

- ( void ) setupTexts {
//    self.playerCountdownTitle.textColor = [[PVPConfigHelper sharedInstance] primaryTextColor];
//    self.playerCountdownDaysLabel.textColor = [[PVPConfigHelper sharedInstance] primaryTextColor];
//    self.playerCountdownHoursLabel.textColor = [[PVPConfigHelper sharedInstance] primaryTextColor];
//    self.playerCountdownMinutesLabel.textColor = [[PVPConfigHelper sharedInstance] primaryTextColor];
//    self.playerCountdownSecondsLabel.textColor = [[PVPConfigHelper sharedInstance] primaryTextColor];
//    self.playerCountdownTimer.textColor = [[PVPConfigHelper sharedInstance] primaryTextColor];
    
    self.playerCountdownTitle.textColor = [UIColor whiteColor];
    self.playerCountdownDaysLabel.textColor = [UIColor whiteColor];
    self.playerCountdownHoursLabel.textColor = [UIColor whiteColor];
    self.playerCountdownMinutesLabel.textColor = [UIColor whiteColor];
    self.playerCountdownSecondsLabel.textColor = [UIColor whiteColor];
    self.playerCountdownTimer.textColor = [UIColor whiteColor];
    
    self.playerCountdownTitle.text = [@"The Stream Will Begin In:" localizedString];
    self.playerCountdownDaysLabel.text = [@"Days" localizedString];
    self.playerCountdownHoursLabel.text = [@"Hours" localizedString];
    self.playerCountdownMinutesLabel.text = [@"Minutes" localizedString];
    self.playerCountdownSecondsLabel.text = [@"Seconds" localizedString];
}

- ( void ) playEntryWithOffset: ( int ) offset {
    
    self.currentOffset = offset < 0 ? 0 : offset;
    
    self.liveIndicatorIcon.hidden = self.currentOffset == 0 ? NO : YES;
    self.liveIndicatorLabel.text = self.currentOffset == 0 ? @"Live" : [NSString stringWithFormat:@"%@ %@", @"-", [PVPTimeHelper formattedTimeForTotalSeconds:self.currentOffset]];
    
    [self setDvrHlsOffsetUrl];
}

- ( void ) playEntry: ( PVPEntry * ) entry withType: ( BambooGalleryType ) type {
    if ( [ entry isKindOfClass: [PVPChannel class] ] ) {
        self.isLiveTimer = [NSTimer scheduledTimerWithTimeInterval:10
                                                            target: self
                                                          selector: @selector(updateIsLive:)
                                                          userInfo: nil
                                                           repeats: YES];// TODO: what if user rewinds channel? still live?
        [self   playChannel: ( PVPChannel * ) entry];
    }
    else {
        sent25Percent = NO;
        sent50Percent = NO;
        sent75Percent = NO;
        sent100Percent = NO;
        
        self.entry = entry;
        self.channel = nil;
        self.galleryType = type;
        [self updateNowPlayingIfNeeded];
        
        if ( [ [PVPConfigHelper sharedInstance] currentConfig ] ) {
            [self initPlayer];
        }
        else {
            [self.configModel initConfig];
        }
        
        if ( self.galleryType == BambooGalleryTypeVOD || self.galleryType == BambooGalleryTypeNode || self.galleryType == BambooGalleryTypeSearchNode ) {
            //Watching timer for VOD
            self.updateWatchingTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(updateCurrentlyWatching:) userInfo:nil repeats:YES];
        }
        else if ( self.galleryType == BambooGalleryTypeLive ) {
            self.isLiveTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(updateIsLive:) userInfo:nil repeats:YES];
            [self setPlayerControlsLiveMode];
        }
    }
    //    [self sendAnalyticsEventWithType:KalturaAnalyticsEventTypePlay withCurrentTime:0];
    //    [self sendGoogleAnalyticsEventWithAction:@"Play" withCurrentTime:0];
}

- ( void ) playEntryWithId: ( NSString * ) entryId {
    [self.entryModel get:entryId];
}

- ( void ) playChannel: ( PVPChannel * ) channel {
    sent25Percent = NO;
    sent50Percent = NO;
    sent75Percent = NO;
    sent100Percent = NO;
    
    //    self.vrVideoView.hidden = YES;
    
    [ self setControlsRevealed: NO ];
    self.shouldShowChannelAds = ![self.channel isEqual:channel];
    
    if (self.entry != nil && [channel.entry.id isEqualToString:self.entry.id]) {
        self.shouldShowChannelAds = NO;
    }
    self.entry = nil;
    self.channel = channel;
    [self updateNowPlayingIfNeeded];
    
    self.galleryType = BambooGalleryTypeChannels;
    
    if ( [ [PVPConfigHelper sharedInstance] currentConfig ] ) {
        [self initPlayer];
    }
    else {
        [self.configModel initConfig];
    }
    
    [self setPlayerControlsLiveMode];
}

- ( void ) playChannelWithId: ( NSString * ) channelId {
    [self.channelModel get:channelId];
}

- ( void ) setPlayPauseButtons: ( BOOL ) isPlay { // true means selected means pause icons gonna show
    self.playPauseToggleButton.selected = isPlay;
    self.livePlayPauseToggleButton.selected = isPlay;
    self.playPauseToggleButtonLarge.selected = isPlay;
}

- ( void ) setPlayerControlsLiveMode {
    
    self.playerControlsView.hidden = YES;
    
    self.livePlayerControlsView.hidden = NO;
    self.livePlayerLogo.hidden = NO;
    self.livePlayPauseToggleButton.hidden = NO;
    self.liveIndicatorContainerView.hidden = NO;
    
    if ( [ [PVPConfigHelper sharedInstance] catchupEnabled ] ) {
        self.forwardButton.hidden = NO;
        self.rewindButton.hidden = NO;
    }
    else {
        self.forwardButton.hidden = YES;
        self.rewindButton.hidden = YES;
    }
}

- ( void ) setPlayerLogoUrl: ( NSString * ) playerLogoUrl {
    _playerLogoUrl = playerLogoUrl;
    if ( playerLogoUrl ) {
        [ self.playerLogo setImageURL: [ NSURL URLWithString: playerLogoUrl ] ];
        self.playerLogoWidthConstraint.constant = playerLogoOriginalWidth;
    }
    else {
        self.playerLogoWidthConstraint.constant = 0;
    }
}

- ( IBAction ) playPauseToggleButtonPressed: ( id ) sender {
    NSLog(@"BambooPlayerView: Play Pause Toggle");
    
    if ( self.playPauseToggleButtonLarge.selected ) { // initial status,
        [ self setControlsRevealed: YES ];
    }
    
    if ( self.controlsRevealed ) {
        [ self setPlayPauseButtons: NO ];
        
        if ( [self isPlaying] ) {
            NSLog(@"BambooPlayerView: isPlaying -> Pause");
            [ self.playPauseToggleButtonLarge setImage: [ UIImage imageNamed: @"pauseIcon" ] forState: UIControlStateNormal ];
            
            [ self pause: YES ];
        }
        else {
            NSLog(@"BambooPlayerView: !isPlaying -> resume");
            [self.playPauseToggleButtonLarge setImage:[UIImage imageNamed:@"playIcon"] forState:UIControlStateSelected];
            [self resume];
        }
        
        [ self setControlsRevealed: NO ];
    }
    else {
        [ self setControlsRevealed: YES ];
    }
    
    [ [NSNotificationCenter defaultCenter] postNotificationName: BAMBOO_PLAYER_TAPPED object: nil ];
}

/// This button action will backward 15 second
- (IBAction)backwardToggleButtonPressed:(id)sender {
    double timeToSeek = CMTimeGetSeconds(self.nativePlayer.currentItem.currentTime);
    timeToSeek -= 15.0;
    if (timeToSeek < 0) {
        [self.nativePlayer seekToTime: kCMTimeZero];
    } else {
        CMTime time = CMTimeMake(timeToSeek, 1);
        [self.nativePlayer seekToTime: time];
    }
}

/// This button action will forward 15 second
- (IBAction)forwardToggleButtonPressed:(id)sender {
    double timeToSeek = CMTimeGetSeconds(self.nativePlayer.currentItem.currentTime);
    timeToSeek += 15.0;
    if (timeToSeek > CMTimeGetSeconds(self.nativePlayer.currentItem.duration)) {
        [self.nativePlayer seekToTime: self.nativePlayer.currentItem.duration];
    } else {
        CMTime time = CMTimeMake(timeToSeek, 1);
        [self.nativePlayer seekToTime: time];
    }
}
/// This button action will start video from start
- (IBAction)startOverPressed:(id)sender {
    [self.nativePlayer seekToTime: kCMTimeZero];
    [self.continueInlinePopUp setHidden:true];
}

- ( void ) setDvrForward {
    
    self.currentOffset = self.currentOffset - 30;
    
    if ( self.currentOffset <= 0 ) {
        self.currentOffset = 0;
        self.liveIndicatorContainerView.hidden = NO;
        self.liveIndicatorIcon.hidden = NO;
        self.liveIndicatorLabel.text = [@"Live" localizedString];
    }
    else {
        self.liveIndicatorIcon.hidden = YES;
        
        self.liveIndicatorLabel.text = [ NSString stringWithFormat: @"%@ %@", @"-", [ PVPTimeHelper formattedTimeForTotalSeconds: self.currentOffset ] ];
        
        [self setDvrHlsOffsetUrl];
    }
    
    [[self delegate] bambooPlayerDidChangeOffset:self.currentOffset];
}

- ( void ) setDvrRewind {
    int maxCatchUpTime = 12*60*60; //Max 12 hours catchup
    self.currentOffset = self.currentOffset + 30;
    
    if ( self.currentOffset < 0 ) {
        self.currentOffset = 0;
    }
    else if ( self.currentOffset > maxCatchUpTime ) { //12 hours into seconds
        self.currentOffset = maxCatchUpTime;
    }
    
    self.liveIndicatorIcon.hidden = self.currentOffset == 0 ? NO : YES;
    self.liveIndicatorLabel.text = self.currentOffset == 0 ? [@"Live" localizedString] : [ NSString stringWithFormat: @"%@ %@", @"-", [PVPTimeHelper formattedTimeForTotalSeconds: self.currentOffset ] ];
    
    [ self setDvrRewindToOffest: self.currentOffset ];
    [[self delegate] bambooPlayerDidChangeOffset:self.currentOffset];
}

- ( void ) setDvrRewindToOffest: ( int ) offset {
    self.currentOffset = offset;
    [self setDvrHlsOffsetUrl];
}

- ( void ) playOrPauseVideoDVR {
    
    if ( self.dvrIsPlay ) {
        self.lastOffsetCreationTime = [ [NSDate date] timeIntervalSince1970 ];
        self.dvrIsPlay = NO;
        [self.nativePlayer pause];
    }
    else {
        int now = [ [NSDate date] timeIntervalSince1970 ];
        int diff = 0;
        if ( self.lastOffsetCreationTime > 0 ) {
            diff = now - self.lastOffsetCreationTime;
        }
        
        self.currentOffset = self.currentOffset + diff;
        [self setDvrHlsOffsetUrl];
        self.dvrIsPlay = YES;
        [self.nativePlayer play];
    }
    
}

- ( void ) setOriginalHlsUrl {
    self.hlsDvrUrl = [self setDvrHlsURL];
    self.currentOffset = 0;
}

- ( NSString * ) setDvrHlsURL {
    
    if ( self.channel && self.channel.entry ) {
        if ( [ ( (PVPLiveEntry * ) self.channel.entry ) respondsToSelector: NSSelectorFromString( @"hlsStreamUrl" ) ] ) {
            return ( (PVPLiveEntry * ) self.channel.entry ).hlsStreamUrl;
        }
    }
    else if ( self.entry.mediaType == [ KalturaMediaType LIVE_STREAM_FLASH ] && [ self.entry isKindOfClass: [ PVPLiveEntry class ] ] ) {
        if ( [ ( ( PVPLiveEntry * ) self.entry ) respondsToSelector: NSSelectorFromString( @"hlsStreamUrl" ) ] ) {
            return ( ( PVPLiveEntry * ) self.entry ).hlsStreamUrl;
        }
    }
    else if ([self.entry isExternalEntry] && [self.entry externalHLSUrl] > 0) {
        return [self.entry externalHLSUrl];
    }
    else if ( [ [PVPConfigHelper sharedInstance] hlsEnabled ] ) {
        return [ PVPEntry hlsManifestUrlWithId: self.entry.id ];
    }
    return self.entry.dataUrl;
}

- ( void ) setDvrHlsOffsetUrl {
    
    NSLog( @"BambooPlayerView: setDvrHlsOffsetUrl" );
    
    if ( ( !_playUrl && self.entry ) || self.channel || ( self.entry.mediaType == [ KalturaMediaType LIVE_STREAM_FLASH ] && [ self.entry isKindOfClass: [ PVPLiveEntry class ] ] ) ) {
        
        NSString * hlsStreamUrl = [self setDvrHlsURL];
        
        if ( [ hlsStreamUrl rangeOfString: self.hlsDvrStreamNameSuffix ].location != NSNotFound ) {
            
            NSArray *urlArray = [ hlsStreamUrl componentsSeparatedByString: @"-" ];
            self.hlsDvrUrl = urlArray[0];
            self.hlsDvrUrl = [ NSString stringWithFormat: @"%@-%d.m3u8", self.hlsDvrUrl, self.currentOffset ];
        }
        else if ( [ hlsStreamUrl rangeOfString: self.hlsAbrStreamNameSuffix ].location != NSNotFound ) {
            
            NSArray *urlArray = [ hlsStreamUrl componentsSeparatedByString:self.hlsAbrBase ];
            NSString *streamName = urlArray[1];
            
            NSArray *streamNameArray = [ streamName componentsSeparatedByString: self.hlsAbrStreamNameSuffix ];
            streamName = streamNameArray[0];
            
            self.hlsDvrUrl = [ self.hlsDvrBaseUrl stringByReplacingOccurrencesOfString: @"STREAM_NAME" withString: streamName];
            NSString *offsetStr = [ NSString stringWithFormat: @"%d", self.currentOffset ];
            self.hlsDvrUrl = [ self.hlsDvrUrl stringByReplacingOccurrencesOfString: @"OFFSET" withString: offsetStr];
        }
        
        self.liveIndicatorIcon.hidden = self.currentOffset == 0 ? NO : YES;
        self.liveIndicatorLabel.text = self.currentOffset == 0 ? @"Live" : [NSString stringWithFormat:@"%@ %@", @"-", [PVPTimeHelper formattedTimeForTotalSeconds:self.currentOffset]];
        
        NSLog( @"BambooPlayerView: Playback url changed to: %@", self.hlsDvrUrl );
        
        _playUrl = self.hlsDvrUrl;
        [ self initNativePlayerWithUrl: _playUrl ];
    }
}
- (IBAction)playerWindowArrangerPressed:(id)sender {
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (UIInterfaceOrientationIsPortrait(interfaceOrientation)) {
        [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeRight) forKey:@"orientation"];
        [UINavigationController attemptRotationToDeviceOrientation];
    } else {
        [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationPortrait) forKey:@"orientation"];
        [UINavigationController attemptRotationToDeviceOrientation];
    }
}

- ( IBAction ) fastforwardButtonPressed: ( id ) sender {
    NSLog( @"BambooPlayerView: Fastforward pressed" );
    
    [ self.playPauseToggleButtonLarge setImage: [ UIImage imageNamed: @"fastforward" ] forState: UIControlStateNormal ];
    [self setDvrForward];
    
    [ [NSNotificationCenter defaultCenter] postNotificationName: BAMBOO_PLAYER_TAPPED object: nil ];
}

- ( IBAction ) rewindButtonPressed: ( id ) sender {
    NSLog( @"BambooPlayerView: Rewind pressed" );
    
    [ self.playPauseToggleButtonLarge setImage: [ UIImage imageNamed: @"rewind" ] forState: UIControlStateNormal ];
    [self initControlRevealTimer];
    [self setDvrRewind];
    
    [ [NSNotificationCenter defaultCenter] postNotificationName: BAMBOO_PLAYER_TAPPED object: nil ];
}
- (IBAction)captionButtonPressed:(id)sender {
    if (self.nativePlayer.currentItem != nil) {
        //    AVMediaCharacteristic
        NSArray * group = self.nativePlayer.currentItem.asset.availableMediaCharacteristicsWithMediaSelectionOptions;
        //    for (AVMediaCharacteristic item in group) {
        ////      NSLocale *local = [[NSLocale init] initWithIdentifier:@"en-EN"];
        //      if (item == AVMediaCharacteristicLegible) {
        //        AVMediaSelectionGroup *group = [self.nativePlayer.currentItem.asset mediaSelectionGroupForMediaCharacteristic: item];
        //        NSArray *options = group.options;
        //        AVMediaSelectionOption *subtitle = group.options.firstObject;
        ////        for (AVMediaSelectionOption *item in options) {
        //          NSLog(@"%d", item);
        ////          if (item.mediaType == AVMediaTypeSubtitle) {
        ////            self.nativePlayer.currentItem select
        //            AVMediaSelectionGroup *subtitleSelectionGroup = [self.nativePlayer.currentItem.asset mediaSelectionGroupForMediaCharacteristic:AVMediaCharacteristicLegible];
        //            if (self.captionButton.isSelected == false) {
        //              [self.captionButton setSelected: true];
        //              [self.nativePlayer.currentItem selectMediaOption:subtitle inMediaSelectionGroup:subtitleSelectionGroup];
        //            } else {
        //              [self.captionButton setSelected: false];
        //              [self.nativePlayer.currentItem selectMediaOption:NULL inMediaSelectionGroup:subtitleSelectionGroup];
        //            }
        ////          }
        ////        }
        //      }
        //    }
        AVMediaSelectionGroup *subtitleSelectionGroup = [self.nativePlayer.currentItem.asset mediaSelectionGroupForMediaCharacteristic:AVMediaCharacteristicLegible];
        if (self.captionButton.isSelected == true) {
            [self.captionButton setSelected:false];
            [self.nativePlayer.currentItem selectMediaOption:NULL inMediaSelectionGroup:subtitleSelectionGroup];
        } else {
            [self.captionButton setSelected:true];
            if (subtitleSelectionGroup) {
                NSUInteger count = subtitleSelectionGroup.options.count;
                AVMediaSelectionOption* option = [subtitleSelectionGroup.options objectAtIndex:0];
                [self.nativePlayer.currentItem selectMediaOption:option inMediaSelectionGroup:subtitleSelectionGroup];
            }
        }
    }
}

- ( void ) destroyControlRevealTimer {
    if ( self.controlsRevealTimer ) {
        [self.controlsRevealTimer invalidate];
        self.controlsRevealTimer = nil;
    }
}

- ( void ) initControlRevealTimer {
    [self destroyControlRevealTimer];
    self.controlsRevealTimer = [ NSTimer scheduledTimerWithTimeInterval: PLAYER_CONTROLS_FADE_OUT_SECONDS target: self selector: @selector( hidePlayerControls: ) userInfo: nil repeats: NO ];
}

- ( void ) resume {
    if ( self.useNativePlayer && self.nativePlayer ) {
        if ((self.galleryType == BambooGalleryTypeChannels || self.galleryType == BambooGalleryTypeLive) && ![[PVPConfigHelper sharedInstance] catchupEnabled ]) {
            [ self.nativePlayer seekToTime: kCMTimeZero ];
            [self.nativePlayer play];
            
        } else {
            [self.nativePlayer play];
        }
    }
    [self setState: BambooPlayerStatePlaying];
    [self setPlayPauseButtons: NO];
    [self sendGoogleAnalyticsEventWithAction: @"Play" withCurrentTime: CMTimeGetSeconds(self.nativePlayer.currentTime)];
    [self showContinueOptionInPlayerView];
}

- (void) pause: ( BOOL ) showControls {
    
    if ( [ [PVPConfigHelper sharedInstance] catchupEnabled ] ) {
        [self playOrPauseVideoDVR];
    }
    
    if ( self.galleryType == BambooGalleryTypeVOD || self.galleryType == BambooGalleryTypeNode || self.galleryType == BambooGalleryTypeSearchNode ) { //Update history on VOD items
        [self updateCurrentlyWatching:self.updateWatchingTimer];
    }
    
    if ( self.adsManager && self.adsManager.adPlaybackInfo && self.adsManager.adPlaybackInfo.playing ) {
        [self.adsManager pause];
    }
    
    if ( self.useNativePlayer && self.nativePlayer ) {
        [self.nativePlayer pause];
    }
    //    else if (self.kPlayer) {
    //        [self.kPlayer pause];
    //    }
    
    [self setState: BambooPlayerStatePaused];
    [self setControlsRevealed:showControls];
    [self setPlayPauseButtons:showControls];
    [self sendGoogleAnalyticsEventWithAction: @"Pause" withCurrentTime: CMTimeGetSeconds(self.nativePlayer.currentTime)];
}

- ( void ) pause {
    [ self pause: YES ];
}

- (void)cleanAds {
    if (self.adsManager) {
        self.adsManager.delegate = nil;
        [self.adsManager destroy];
        self.adsManager = nil;
    }
}

- ( void ) seekToTime: ( int ) time {
    if ( self.useNativePlayer && self.nativePlayer ) {
        CMTime newTime = CMTimeMakeWithSeconds( time, 1 );
        [ self.nativePlayer seekToTime: newTime completionHandler: ^( BOOL finished ) {
            NSLog( @"BambooPlayerView: seek to %@ complete", [ PVPTimeHelper formattedTimeForTotalSeconds: time ] );
            if ([[PVPConfigHelper sharedInstance] isHistoryEnabled]) {
                [self showContinueOptionInPlayerView];
            }
        }];
    }
}
- (void) enableContinuePopUpView {
    if ([[PVPConfigHelper sharedInstance] isHistoryEnabled]) {
        self.showContinuePopUp = YES;
        if (_adIsFinished == YES) {
            [self showContinueOptionInPlayerView];
        }
    } else {
        self.showContinuePopUp = NO;
    }
}
-(void) showContinueOptionInPlayerView {
    if (self.showContinuePopUp == YES) {
        self.showContinuePopUp = NO;
        [self.continueInlinePopUp setHidden:false];
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self.continueInlinePopUp setHidden:true];
        });
    }
}

- ( int ) currentTime {
    return CMTimeGetSeconds( self.nativePlayer.currentTime );
}

- ( BOOL ) isPlaying {
    BOOL isPlaying = NO;
    
    if ( self.useNativePlayer && self.nativePlayer ) {
        isPlaying = self.nativePlayer != nil && self.nativePlayer.rate != 0 && self.nativePlayer.error == nil;
    }
    return isPlaying;
}

#pragma content loading
- ( void ) initPlayerLoader {
    CGSize thumbnailSize = CGSizeMake( self.bounds.size.width, self.bounds.size.height );
    NSURL* imageUrl = nil;
    NSString* playerLogoUrl = nil;
    
    self.thumbImage.alpha = 0.8f;
    self.thumbImage.crossfadeDuration = self.preloadedImage ? ( NSTimeInterval ) 0 : ( NSTimeInterval ) 0.5;
    self.thumbImage.contentMode = UIViewContentModeScaleAspectFill;
    [ self showLoadingHUD: @"" ];
    
    switch ( self.galleryType ) {
        case BambooGalleryTypeOffline:
            self.thumbImage.image = [ [FileDownloadManager sharedInstance] localImageForEntry: self.entry ];
            self.playbackSeekSlider.maximumValue = self.entry.duration;
            break;
        case BambooGalleryTypeChannels:
            imageUrl = [ NSURL URLWithString: [ PVPEntry kalturaThumbnailUrlForEntryId: self.channel.entry.id withSize: thumbnailSize withType: @"3" ] ];
            break;
            //        case BambooGalleryTypeNode:
            //            imageUrl = [NSURL URLWithString:[PVPEntry kalturaThumbnailUrlForEntryId:self.node.videoId withSize:thumbnailSize withType:@"3"]];
            //            break;
        default:
            // load static preloaded image from parent view
            imageUrl = [ NSURL URLWithString: [ PVPEntry kalturaThumbnailUrlForEntryId: self.entry.id withSize: thumbnailSize withType: @"3"] ];
            self.playbackSeekSlider.maximumValue = self.entry.duration;
            break;
    }
    
    if ( self.preloadedImage && self.galleryType != BambooGalleryTypeOffline ) {
        [ self.thumbImage setImage: self.preloadedImage ];
    }
    else if ( imageUrl ) {
        [ self.thumbImage setImageURL:imageUrl ];
    }
    
    if ( self.channel && self.channel.playerLogoUrl && self.channel.playerLogoUrl.length > 0 ) {
        playerLogoUrl = self.channel.playerLogoUrl;
    }
    else if ( self.entry && self.entry.info.playerLogoUrl && self.entry.info.playerLogoUrl.length > 0 ) {
        playerLogoUrl = self.entry.info.playerLogoUrl;
    }
    else {
        playerLogoUrl = [ [PVPConfigHelper sharedInstance] defaultPlayerLogoUrl ];
    }
    
    [ self setPlayerLogoUrl: playerLogoUrl ];
    [self layoutIfNeeded];
}

- ( void ) initPlayer {
    [self setupColors];
    
    // Enable watermark if needed
    NSDictionary *mobile = [[PVPConfigHelper sharedInstance] currentConfig].mobile;
    if (mobile != nil) {
        NSDictionary *player = mobile[@"player"];
        if (player != nil && [player isKindOfClass:[NSDictionary class]]) {
            NSString *imageUrl = player[@"watermark"];
            if (imageUrl != nil) {
                _watermarkImageView.hidden = NO;
                [_watermarkImageView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            }
        }
    }
    
    self.hlsDvrBaseUrl = [PVPConfigHelper sharedInstance].currentConfig.mobile[ @"hlsDvrBaseUrl" ];
    self.hlsAbrBase = [PVPConfigHelper sharedInstance].currentConfig.mobile[ @"hlsAbrBase" ];
    self.hlsAbrStreamNameSuffix = @"_abr/";
    self.hlsDvrStreamNameSuffix = @"_dvr/";
    
    self.dvrIsPlay = YES;
    self.showContinuePopUp = NO; /// by defaut show pop is false
    self.lastOffsetCreationTime = 0;
    self.currentOffset = 0;
    
    self.entryId = nil;
    _playUrl = nil;
    
    [ [NSNotificationCenter defaultCenter] removeObserver: self name: UIApplicationWillTerminateNotification object: nil ];
    [ [NSNotificationCenter defaultCenter] removeObserver: self name: UIDeviceOrientationDidChangeNotification object: nil ];
    
    [ [NSNotificationCenter defaultCenter] addObserver: self selector: @selector( appWillTerminate: ) name: UIApplicationWillTerminateNotification object: nil ];
    
    
    BOOL isGenDeviceOrientNotif = [ [UIDevice currentDevice] isGeneratingDeviceOrientationNotifications ];
    if ( !isGenDeviceOrientNotif ) {
        [ [UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications ];
    }
    [ [NSNotificationCenter defaultCenter] addObserver: self selector: @selector( orientationChanged: ) name: UIDeviceOrientationDidChangeNotification object: nil ];
    
    if ( self.updateWatchingTimer ) {
        [self.updateWatchingTimer invalidate];
    }
    if ( self.isLiveTimer ) {
        [self.isLiveTimer invalidate];
    }
    
    BOOL startNow = YES;
    NSDate *now = [NSDate date];
    BOOL hlsEnabled = [ [PVPConfigHelper sharedInstance] hlsEnabled ];
    
    self.useNativePlayer = YES;
    
    switch ( self.galleryType ) {
        case BambooGalleryTypeOffline:
            if ( self.entry ) {
                self.entryId = self.entry.id;
                _playUrl = [ [FileDownloadManager sharedInstance] localPlaybackUrlForEntry: self.entry ];
            }
            break;
        case BambooGalleryTypeChannels:
            if ( self.channel ) {
                self.entryId = self.channel.entry.id;
                _playUrl = self.channel.hlsUrl;
            }
            break;
        default:
            if ( self.entry ) {
                self.entryId = self.entry.id;
                if ( self.entry.mediaType == [ KalturaMediaType LIVE_STREAM_FLASH ] && [ self.entry isKindOfClass: [ PVPLiveEntry class ] ] ) {
                    _playUrl = ( ( PVPLiveEntry * ) self.entry ).hlsStreamUrl;
                }
                else if ([self.entry isExternalEntry] && [self.entry externalHLSUrl] > 0) {
                    _playUrl = [self.entry externalHLSUrl];
                }
                else if ( hlsEnabled ) {
                    _playUrl = [ PVPEntry hlsManifestUrlWithId: self.entry.id ];
                }
                else {
                    _playUrl = self.entry.dataUrl;
                }
                
                NSDate *entryStartDate = [ NSDate dateWithTimeIntervalSince1970: self.entry.startDate ];
                if ( [entryStartDate timeIntervalSince1970] > [now timeIntervalSince1970WithServerOffset] && ![ [ [ApiManager sharedInstance] loggedInUser ] bypassSchedulingRestriction ] )
                {
                    startNow = NO;
                    self.liveCountdownTimer = [ [MZTimerLabel alloc] initWithLabel: self.playerCountdownTimer andTimerType: MZTimerLabelTypeTimer ];
                    [ self.liveCountdownTimer setCountDownTime: [entryStartDate timeIntervalSince1970] - [now timeIntervalSince1970WithServerOffset] + 2 ];
                    self.liveCountdownTimer.delegate = self;
                    [ self.liveCountdownTimer startWithEndingBlock: ^( NSTimeInterval countTime ) {
                        @try {
                            if ( self.playerCountdownContainer ) {
                                [self.playerCountdownContainer fadeOut];
                            }
                            [ self showLoadingHUD: @"" ];
                            //                            if (self.playerSpinner) {
                            //                                [self.playerSpinner fadeIn];
                            //                            }
                            if ( self.liveCountdownTimer ) {
                                [self.liveCountdownTimer pause];
                                self.liveCountdownTimer = nil;
                            }
                            [self initPlayer];
                        } @catch ( NSException *exception ) {
                            [self cannotPlay];
                        } @finally {
                            
                        }
                    } ];
                    
                    if ( self.playerCountdownContainer ) {
                        [self.playerCountdownContainer fadeIn];
                    }
                    //                    if (self.playerSpinner) {
                    //                        [self.playerSpinner fadeOut];
                    //                    }
                    [ progressDialog hideAnimated: true ];
                }
            }
            break;
    }
    
    [self initPlayerLoader];
    
    NSDate *entryEndDate = [ NSDate dateWithTimeIntervalSince1970: self.entry.endDate ];
    if ( self.entry.endDate != 0 && [entryEndDate timeIntervalSince1970] < [now timeIntervalSince1970WithServerOffset] ) {
        [self streamEnded];
    }
    else if ( [ [PVPConfigHelper sharedInstance] isCountryAllowed ]
             || ( [ [ApiManager sharedInstance] loggedInUser ]
                 && ( [ [ [ApiManager sharedInstance] loggedInUser ] bypassCountryRestriction ]
                     || [ [ [ApiManager sharedInstance] loggedInUser ].pvpRole isEqualToString: PVP_ROLE_ADMIN ]
                     || [ [ [ApiManager sharedInstance] loggedInUser ].pvpRole isEqualToString: PVP_ROLE_ROOT ]
                     || [ [ [ApiManager sharedInstance] loggedInUser ].pvpRole isEqualToString: PVP_ROLE_MANAGER ] ) ) )
    {// very long if opening bracket
        if ( startNow ) {
            if ( self.useNativePlayer ) {
                if ( [ [PVPConfigHelper sharedInstance] drmEnabled ] ) {
                    [self setupDrm];
                }
                else {
                    [ self initNativePlayerWithUrl: _playUrl ];
                }
                [ self setControlsRevealed: NO ];
            }
        }
        else {
            [ self setControlsRevealed: YES ];
        }
    }
    else {
        [self blockByCountry];
    }
}

- ( void ) streamEnded {
    NSLog( @"BambooPlayerView: Stream Has ended" );
    [ progressDialog hideAnimated: true ];
    
    //    if ([PVPConfigHelper alertsEnabled]) {
    [ PVPAlertHelper showAlertWithTitle: [@"Stream Ended" localizedString]
                             andMessage: [@"This stream has ended." localizedString]
                        withButtonTitle: [ALERT_OK localizedString]
                            withHandler: ^( UIAlertController * _Nonnull alert, BOOL confirmed ) {
        [ self setState: BambooPlayerStateWillNotPlay ];
    } ];
    //    } else {
    //        [self setState:BambooPlayerStateWillNotPlay];
    //    }
}

- ( void ) blockByCountry {
    [ FIRAnalytics logEventWithName: kFIREventBlockedCountry parameters: nil ];
    
    NSLog( @"BambooPlayerView: Country is blocked" );
    [ progressDialog hideAnimated: true ];
    //    if ([PVPConfigHelper alertsEnabled]) {
    [ PVPAlertHelper showAlertWithTitle: [@"Country Blocked" localizedString]
                             andMessage: [@"This video is not available in your region." localizedString]
                        withButtonTitle: [ALERT_OK localizedString]
                            withHandler: ^( UIAlertController * _Nonnull alert, BOOL confirmed ) {
        [ self setState: BambooPlayerStateWillNotPlay ];
    } ];
    //    } else {
    //        [self setState:BambooPlayerStateWillNotPlay];
    //    }
}

- ( void ) cannotPlay {
    NSLog( @"BambooPlayerView: Cannot play" );
    
    [ progressDialog hideAnimated: true ];
    //    if ([PVPConfigHelper alertsEnabled]) {
    [ PVPAlertHelper showAlertWithTitle: [@"Playback Error" localizedString]
                             andMessage: [@"There has been an issue while trying to play this video. Please try again later." localizedString]
                        withButtonTitle: [ALERT_OK localizedString]
                            withHandler: ^( UIAlertController * _Nonnull alert, BOOL confirmed ) {
        [ self setState: BambooPlayerStateWillNotPlay ];
    } ];
    //    } else {
    //        [self setState:BambooPlayerStateWillNotPlay];
    //    }
}

- ( NSURL * ) tokenizePlaybackUrlWithUrl: ( NSURL * ) url {
    NSURL *newUrl = url;
    if ( self.entry != nil && self.entry.mediaType == [KalturaMediaType LIVE_STREAM_FLASH] ) {
        NSString *newUrlString = [ self tokenizePlaybackUrl: url.absoluteString ];
        newUrl = [ NSURL URLWithString: newUrlString ];
    }
    return newUrl;
}

- ( NSString * ) tokenizePlaybackUrl: ( NSString * ) url {
    
    NSString *newUrl = url;
    
    if ( self.entry != nil && self.entry.mediaType == [KalturaMediaType LIVE_STREAM_FLASH] ) {
        NSString *publicIP = [ NSString stringWithContentsOfURL: [ NSURL URLWithString: @"http://checkip.amazonaws.com/" ] encoding: NSUTF8StringEncoding error: nil ];
        publicIP = [ publicIP stringByTrimmingCharactersInSet: [NSCharacterSet newlineCharacterSet] ]; // IP comes with a newline for some reason
        
        NSString *validminutes = @"1";
        NSTimeZone *timeZone = [ NSTimeZone timeZoneWithAbbreviation: @"GMT" ];
        NSDateFormatter *dateFormatter = [ [NSDateFormatter alloc] init ];
        [ dateFormatter setTimeZone: timeZone ];
        
        [ dateFormatter setDateFormat: @"M/dd/yyyy h:mm:ss a" ];
        NSString *today = [ dateFormatter stringFromDate: [NSDate date] ];
        //Today example format: 7/30/2018 7:29:04 AM
        
        NSString *token = [ PVPConfigHelper getLocalConfiguration: @"tokenizationSharedSecret" ];
        NSString *to_hash = [ NSString stringWithFormat: @"%@%@%@%@", publicIP, token, today, validminutes ];
        NSLog( @"BambooPlayerView: string to Hash: %@", to_hash );
        
        NSString *to_hash_md5 = [ AppUtils md5HexDigest: to_hash withRaw: YES ];
        NSLog( @"BambooPlayerView: string to Hash MD5: %@", to_hash_md5 );
        
        NSData *to_hash_md52 = [ to_hash MD5CharData ];
        NSLog( @"BambooPlayerView: NSDATA to Hash: %@", to_hash_md52 );
        
        NSData *data = [ to_hash_md5 dataUsingEncoding: NSUTF8StringEncoding ];
        NSLog( @"BambooPlayerView: some unused var: %@", data );
        
        // Get NSString from NSData object in Base64
        NSString *base64Encoded = [ to_hash_md52 base64EncodedStringWithOptions: 0 ];
        NSLog( @"BambooPlayerView: string base64encoded: %@", base64Encoded );
        
        NSString *urlsignature = [ NSString stringWithFormat: @"server_time=%@&hash_value=%@&validminutes=%@", today, base64Encoded, validminutes ];
        NSData *signatureData = [ urlsignature dataUsingEncoding: NSUTF8StringEncoding ];
        
        // Get NSString from NSData object in Base64
        NSString *signatureBase64Encoded = [ signatureData base64EncodedStringWithOptions: 0 ];
        NSLog( @"BambooPlayerView: string signatureBase64Encoded: %@", signatureData );
        
        newUrl = [ NSString stringWithFormat: @"%@?wmsAuthSign=%@", url, signatureBase64Encoded ];
        NSLog( @"BambooPlayerView: New Tokenized url is: %@", newUrl );
    }
    return newUrl;
}

- (void)hideThumbnailIfNeeded {
    if (self.entry.mediaType != [KalturaMediaType AUDIO]) {
        [self.thumbImage fadeOut];
    }
}

#pragma mark native player
-  ( void ) initNativePlayerWithUrl: ( NSString * ) playUrl {
    // playUrl = @"https://devstreaming-cdn.apple.com/videos/streaming/examples/img_bipbop_adv_example_fmp4/master.m3u8";
    NSString *deviceType = [ [ AppUtils getDefaultValueFofKey: IS_SIMULATOR_KEY ] boolValue ] ? @"Simulator" : @"Device";
    UIDevice *device = [UIDevice currentDevice];
    NSString *deviceInfo = [ NSString stringWithFormat: @"Apple %@ %@ %@ %@", device.model, device.systemName, device.systemVersion, deviceType ];
    AVAsset *asset;
    //    NSString *userAgent = [AppUtils getDeviceUserAgent];
    NSString *userAgent = @"iOS";
    playUrl = [self formattedTagsUrlStringForString: playUrl];
    if ( [ [PVPConfigHelper sharedInstance] tokenizationEnabled ] && self.entry.mediaType == [KalturaMediaType LIVE_STREAM_FLASH] ) {
        playUrl = [ self tokenizePlaybackUrl: playUrl ];
    }
    NSLog(@"Now Playing URL %@", playUrl);
    switch ( self.galleryType ) {
        case BambooGalleryTypeOffline:
            asset = [ AVAsset assetWithURL: [ NSURL fileURLWithPath: playUrl ] ];
            _nativePlayerItem = [ AVPlayerItem playerItemWithAsset: asset ];
            self.nativePlayer = [ [AVPlayer alloc] initWithPlayerItem: _nativePlayerItem ];
            break;
        default:
            
            //            asset = [AVAsset assetWithURL:[NSURL URLWithString:playUrl]];
            asset = [ AVURLAsset URLAssetWithURL: [ NSURL URLWithString: playUrl ] options: @{ @"AVURLAssetHTTPHeaderFieldsKey" :
                                                                                                   @{
                                                                                                       @"User-Agent": userAgent,
                                                                                                       @"bamboo-req": deviceInfo
                                                                                                   }
                                                                                            } ];
            _nativePlayerItem = [ AVPlayerItem playerItemWithAsset: asset ];
            if ( _nativePlayerLayer == nil ) {
                self.nativePlayer = [ [AVPlayer alloc] initWithPlayerItem: _nativePlayerItem ];
                [ [NSNotificationCenter defaultCenter] addObserver: self selector: @selector( itemDidFinishPlaying: ) name: AVPlayerItemDidPlayToEndTimeNotification object: _nativePlayerItem ];
                
            }
            else {
                [self.nativePlayer pause];
                [self.nativePlayer replaceCurrentItemWithPlayerItem:_nativePlayerItem];
            }
            break;
    }
    [self setUpCaption];
    [ self configureNativePlayer: YES ];
}

- ( void ) itemDidFinishPlaying: ( NSNotification * ) notification {
    
    // Will be called when AVPlayer finishes playing playerItem
    [ [NSOperationQueue mainQueue] addOperationWithBlock: ^{ //posting NSNotifications must be done on the main thread
        [ [NSNotificationCenter defaultCenter] postNotificationName: BAMBOO_PLAYER_FINISHED object: nil ];
    } ];
}

- ( NSURL * ) urlOfCurrentlyPlayingInPlayer: ( AVPlayer * ) player {
    // get current asset
    AVAsset *currentPlayerAsset = player.currentItem.asset;
    // make sure the current asset is an AVURLAsset
    if ( ![currentPlayerAsset isKindOfClass: AVURLAsset.class ] ) return nil;
    // return the NSURL
    return [ (AVURLAsset * )currentPlayerAsset URL ];
}

- ( void ) initNativePlayerWithAsset: ( AVURLAsset * ) asset {
    
    NSString *formattedAssetString = [self formattedTagsUrlStringForString: [[asset URL] absoluteString]];
    NSURL *formattedAssetURL = [NSURL URLWithString:formattedAssetString];
    if (formattedAssetURL != nil) {
        asset = [asset initWithURL: formattedAssetURL options: nil];
    }
    
    if ( [ [PVPConfigHelper sharedInstance] tokenizationEnabled ] && self.entry.mediaType == [KalturaMediaType LIVE_STREAM_FLASH] ) {
        NSURL *newUrl = [ self tokenizePlaybackUrlWithUrl: [asset URL] ];
        asset = [ asset initWithURL: newUrl options: nil ];
    }
    
    @try {
        /* Stop observing our prior AVPlayerItem, if we have one. */
        if ( _nativePlayerItem ) {
            /* Remove existing player item key value observers. */
            [ _nativePlayerItem removeObserver: self forKeyPath: BAMBOO_PLAYER_STATUS_KEY ];
            [ _nativePlayerItem removeObserver: self forKeyPath: AVPlayerItemDidPlayToEndTimeNotification ];
        }
    } @catch ( NSException *exception ) {
        NSLog( @"BambooPlayerView: FAIL - _nativePlayerItem - removing (for player init) observers on 'BAMBOO_PLAYER_STATUS_KEY and AVPlayerItemDidPlayToEndTimeNotification': %@", exception );
    } @finally {
        /* Create a new instance of AVPlayerItem from the now successfully loaded AVAsset. */
        _nativePlayerItem = [ AVPlayerItem playerItemWithAsset: asset ];
        /* Observe the player item "status" key to determine when it is ready to play. (NSObject feature) */
        [ _nativePlayerItem addObserver: self
                             forKeyPath: BAMBOO_PLAYER_STATUS_KEY
                                options: NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                                context: AVPlayerStatusObservationContext ];
        
        [ [NSNotificationCenter defaultCenter] addObserver: self selector: @selector( itemDidFinishPlaying: ) name: AVPlayerItemDidPlayToEndTimeNotification object: _nativePlayerItem ];
        
        if ( _nativePlayerLayer == nil ) {
            self.nativePlayer = [ [AVPlayer alloc] initWithPlayerItem: _nativePlayerItem ];
            
        }
        else {
            [ self.nativePlayer pause ];
            [ self.nativePlayer replaceCurrentItemWithPlayerItem: _nativePlayerItem ];
        }
        
        /* When an iOS device is in AirPlay mode, FPS content will not play on an attached AppleTV
         unless AirPlay playback is set to full screen. */
        self.nativePlayer.usesExternalPlaybackWhileExternalScreenIsActive = YES;
        
        /* Make our new AVPlayerItem the AVPlayer's current item. */
        if ( self.nativePlayer.currentItem != _nativePlayerItem ) {
            /* Replace the player item with a new player item. The item replacement occurs
             asynchronously; observe the currentItem property to find out when the
             replacement will/did occur
             
             If needed, configure player item here (example: adding outputs, setting text style rules,
             selecting media options) before associating it with a player
             */
            [ self.nativePlayer replaceCurrentItemWithPlayerItem: _nativePlayerItem ];
        }
        [ self configureNativePlayer: NO ];
    }
}

- ( void ) setVolume: ( float ) volume {
    _volume = volume;
    [self.nativePlayer setVolume:volume];
}

- ( void ) setMute: ( BOOL ) mute {
    if (mute) {
        [[PVPPlayerAnalyticsManager sharedInstance] logEvent:PVPPlayerMuteEvent];
    } else {
        [[PVPPlayerAnalyticsManager sharedInstance] logEvent:PVPPlayerUnmuteEvent];
    }
    
    [self.nativePlayer setVolume: mute ? 0.0 : self.volume];
}

- ( BOOL ) isMuted {
    return self.nativePlayer.volume == 0;
}

- ( void ) configureNativePlayer: ( BOOL ) playNow {
    if (_nativePlayerLayer != nil) {
        [_nativePlayerLayer removeFromSuperlayer];
    }
    
    _nativePlayerLayer = [ AVPlayerLayer playerLayerWithPlayer: self.nativePlayer ];
    _nativePlayerLayer.videoGravity = AVLayerVideoGravityResize;
  
    
    [self updateNativePlayerLayerFrame];
    
    
    [ self.videoView.layer addSublayer: _nativePlayerLayer ];
    
    _nativePlayerLayer.frame = self.videoView.bounds;
    [self.videoView layoutIfNeeded];
    
    if ([[PVPConfigHelper sharedInstance] allowBackgroundPlayback]) {
        [self setupRemoteControls];
        [self linkPip:_nativePlayerLayer];
    }
    
    
    [ self.nativePlayer seekToTime: kCMTimeZero ];
    self.nativePlayer.volume = self.volume;
    self.nativePlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    @try {
        [ self.nativePlayer removeObserver: self forKeyPath: @"rate" ];
        [ self.nativePlayer.currentItem removeObserver: self forKeyPath: @"status"];
    } @catch ( NSException *exception ) {
    } @finally {
        [ self.nativePlayer addObserver: self forKeyPath: @"rate" options: 0 context: 0 ];
        [ self.nativePlayer.currentItem addObserver: self forKeyPath: @"status" options: 0 context: 0 ];
        
        if ( playNow ) {
            [self startNativePlayerPlayback];
        }
        else {
            [self setControlsRevealed:YES];
        }
    }
    
    __unsafe_unretained BambooPlayerView *weakSelf = self;
    
    if ( self.galleryType == BambooGalleryTypeVOD || self.galleryType == BambooGalleryTypeOffline || self.galleryType == BambooGalleryTypeCategory || self.galleryType == BambooGalleryTypeCategories || self.galleryType == BambooGalleryTypeWatchHistory || self.galleryType == BambooGalleryTypeWatchHistoryTree || self.galleryType == BambooGalleryTypeNode || self.galleryType == BambooGalleryTypeSearchNode|| self.galleryType == BambooGalleryTypeSearchEntry ) {
        
        self.timeObserverBlockUpdateProgressBar = ^( CMTime time ) {
            [weakSelf updateProgressBar: CMTimeGetSeconds( time ) ];
        };
        
        self.playerObserverUpdateProgressBar = [ self.nativePlayer addPeriodicTimeObserverForInterval: CMTimeMake( 1, 33 ) queue: dispatch_get_main_queue() usingBlock: self.timeObserverBlockUpdateProgressBar ];
        
        self.liveIndicatorContainerView.hidden = YES;
    }
    
    self.timeObserverBlockUpdateVideoProgressStats = ^( CMTime time ) {
        if ( self->_nativePlayerLayer.videoRect.size.height > 0 && self->_nativePlayerLayer.videoRect.size.width > 0 && !CGRectEqualToRect( self->_videoRect, self->_nativePlayerLayer.videoRect ) )
        {
            NSLog( @"BambooPlayerView: video frame changed: %@", NSStringFromCGRect( weakSelf.nativePlayerLayer.videoRect ) );
            
            self->_videoRect = self->_nativePlayerLayer.videoRect;
            if ( self.delegate && [ self.delegate respondsToSelector: @selector( bambooPlayerVideoRectDidChange: ) ] ) {
                [ weakSelf.delegate bambooPlayerVideoRectDidChange: weakSelf.videoRect ];
            }
        }
        
        if ( weakSelf.playbackSeekSlider.value >= weakSelf.playbackSeekSlider.maximumValue * 0.25 && !self->sent25Percent) {
            self->sent25Percent = YES;
            [[PVPPlayerAnalyticsManager sharedInstance] logEvent:PVPPlayerFirstQuartileEvent];
            [ weakSelf sendAnalyticsEventWithType: KalturaAnalyticsEventTypePlayReached25 withCurrentTime: CMTimeGetSeconds( weakSelf.nativePlayer.currentTime ) ];
            [ weakSelf sendGoogleAnalyticsEventWithAction: @"PlayReached25" withCurrentTime: CMTimeGetSeconds( weakSelf.nativePlayer.currentTime ) ];
        }
        else if ( weakSelf.playbackSeekSlider.value >= weakSelf.playbackSeekSlider.maximumValue * 0.5 && !self->sent50Percent ) {
            self->sent50Percent = YES;
            [[PVPPlayerAnalyticsManager sharedInstance] logEvent:PVPPlayerMidPointEvent];
            [ weakSelf sendAnalyticsEventWithType: KalturaAnalyticsEventTypePlayReached50 withCurrentTime: CMTimeGetSeconds( weakSelf.nativePlayer.currentTime ) ];
            [ weakSelf sendGoogleAnalyticsEventWithAction: @"PlayReached50" withCurrentTime: CMTimeGetSeconds( weakSelf.nativePlayer.currentTime ) ];
        }
        else if ( weakSelf.playbackSeekSlider.value >= weakSelf.playbackSeekSlider.maximumValue * 0.75 && !self->sent75Percent ) {
            self->sent75Percent = YES;
            [[PVPPlayerAnalyticsManager sharedInstance] logEvent:PVPPlayerThirdQuartileEvent];
            [ weakSelf sendAnalyticsEventWithType: KalturaAnalyticsEventTypePlayReached75 withCurrentTime: CMTimeGetSeconds( weakSelf.nativePlayer.currentTime ) ];
            [ weakSelf sendGoogleAnalyticsEventWithAction: @"PlayReached75" withCurrentTime: CMTimeGetSeconds( weakSelf.nativePlayer.currentTime ) ];
        }
        else if ( weakSelf.playbackSeekSlider.value >= weakSelf.playbackSeekSlider.maximumValue * 0.95 && !self->sent100Percent ) {
            self->sent100Percent = YES;
            [[PVPPlayerAnalyticsManager sharedInstance] logEvent:PVPPlayerCompleteEvent];
            [ weakSelf sendAnalyticsEventWithType: KalturaAnalyticsEventTypePlayReached100 withCurrentTime: CMTimeGetSeconds( weakSelf.nativePlayer.currentTime ) ];
            [ weakSelf sendGoogleAnalyticsEventWithAction: @"PlayReached100" withCurrentTime: CMTimeGetSeconds( weakSelf.nativePlayer.currentTime ) ];
        }
    };
    
    self.playerObserverUpdateVideoProgressStats = [ self.nativePlayer addPeriodicTimeObserverForInterval: CMTimeMake( 1, 2 ) queue: dispatch_get_main_queue() usingBlock: self.timeObserverBlockUpdateVideoProgressStats ];
}

- ( void ) startNativePlayerPlayback {
    [ self sendAnalyticsEventWithType: KalturaAnalyticsEventTypeWidgetLoaded withCurrentTime: 0 ];
    [ self sendAnalyticsEventWithType: KalturaAnalyticsEventTypeMediaLoaded withCurrentTime: 0 ];
    [ self sendAnalyticsEventWithType: KalturaAnalyticsEventTypePlay withCurrentTime: 0 ];
    
    [ self sendGoogleAnalyticsEventWithAction: @"WidgetLoaded" withCurrentTime: 0 ];
    [ self sendGoogleAnalyticsEventWithAction: @"MediaLoaded" withCurrentTime: 0 ];
    [ self sendGoogleAnalyticsEventWithAction: @"Play" withCurrentTime: 0 ];
    
    NSString *adTagUrl = [self formattedTagsUrlStringForString: [[PVPConfigHelper sharedInstance] adTagUrl]];
    if ([[PVPConfigHelper sharedInstance] awesomeAdsEnabled] && [PVPLocalizationHelper canUseString:[ [PVPConfigHelper sharedInstance] awesomeAdsPrerollPlacementID]]) {
        // This can be removed
    }
    else if ( adTagUrl && adTagUrl.length > 0 && (self.entry != nil || (self.channel != nil && self.shouldShowChannelAds))) {
        _adIsFinished = NO;
        _shouldShowChannelAds = NO;
        IMASettings *settings = [[IMASettings alloc] init];
        
        // MARK: PIP Enabling PIP for video ads
        if ([[PVPConfigHelper sharedInstance] allowBackgroundPlayback]) {
            settings.enableBackgroundPlayback = YES;
        } else {
            settings.enableBackgroundPlayback = NO;
        }
        
        settings.disableNowPlayingInfo = YES;
        
        _adsLoader = [[IMAAdsLoader alloc] initWithSettings:settings];
        _adsLoader.delegate = self;
        // Set up our content playhead and contentComplete callback.
        _contentPlayhead = [ [IMAAVPlayerContentPlayhead alloc] initWithAVPlayer: self.nativePlayer ];
        [ [NSNotificationCenter defaultCenter] addObserver: self
                                                  selector: @selector( contentDidFinishPlaying: )
                                                      name: AVPlayerItemDidPlayToEndTimeNotification
                                                    object: self.nativePlayer.currentItem ];
        // Create an ad display container for ad rendering.
        
        UIViewController * controller = UIViewParentController(self);
        _adDisplayContainer = [[IMAAdDisplayContainer alloc]  initWithAdContainer:self.videoView
                                                                  viewController:controller
                                                                  companionSlots:nil];
        
         //_adDisplayContainer = [ [IMAAdDisplayContainer alloc] initWithAdContainer: self.videoView companionSlots: nil ];
        // Create an ad request with our ad tag, display container, and optional user context.
        // MARK: PIP use below to disable PIP in ads
        IMAAdsRequest *request = [ [IMAAdsRequest alloc] initWithAdTagUrl: adTagUrl
                                                       adDisplayContainer: _adDisplayContainer
                                                          contentPlayhead: _contentPlayhead
                                                              userContext: nil ];
        
        
        // MARK: PIP use below method for ads to work in PIP mode. (Not working)
        
        //  _videoDisplay = [[IMAAVPlayerVideoDisplay alloc] initWithAVPlayer:self.nativePlayer];
//
//        IMAAdsRequest *request = [[IMAAdsRequest alloc]
//                                  initWithAdTagUrl:adTagUrl
//                                  adDisplayContainer:_adDisplayContainer
//                                  avPlayerVideoDisplay:[[IMAAVPlayerVideoDisplay alloc] initWithAVPlayer:self.nativePlayer]
//                                  pictureInPictureProxy:self.pictureInPictureProxy
//                                  userContext:nil];
        [ _adsLoader requestAdsWithRequest: request ];
    }
    else {
        if ( self.videoView != nil ) {
            [self.nativePlayer play];
        }
    }
}

- ( void ) layoutSubviews {
    [super layoutSubviews];
    _nativePlayerLayer.frame = self.bounds;
}

// make sure to remove image after player starts playing
- ( void ) observeValueForKeyPath: ( NSString * ) keyPath ofObject: ( id ) object change: ( NSDictionary * ) change context: ( void * ) context {
    if ( context == 0 ) {
        NSLog(@"BambooPlayerView: KeyPath %@", keyPath);
        
        if ( [keyPath isEqualToString:@"rate"] ) {
            if ( [self isPlaying] ) {
                NSLog(@"BambooPlayerView: playing at rate: %f", self.nativePlayer.rate);
                //playing
                self.playPauseToggleButtonLarge.selected = NO;
                
                if ( self.channel ||
                    ( self.entry &&
                     ( self.entry.mediaType == [KalturaMediaType LIVE_STREAM_FLASH] || [ self.entry isKindOfClass: [PVPLiveEntry class] ] ) ) )
                {
                    [ self setState: BambooPlayerStatePlaying ];
                }
                if ( self.nativePlayer.currentItem.status == AVPlayerItemStatusReadyToPlay ) {
                    [progressDialog hideAnimated: true];
                }
            }
            else {
                
                [ self setState: BambooPlayerStatePaused ];
            }
        }
        else if ( [keyPath isEqualToString:@"status"] ) {
            [self hideThumbnailIfNeeded];
            
            NSLog(@"BambooPlayerView: status %ld", self.nativePlayer.currentItem.status);
            if ( self.nativePlayer.currentItem.status == AVPlayerItemStatusReadyToPlay ) {
                [ self setState: BambooPlayerStateReady ];
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.videoView.hidden = NO;
                    [self updateNativePlayerLayerFrame];
                });
                //                [ self.videoView.layer addSublayer: _nativePlayerLayer ];
            }
        }
    }
    else if ( context == AVPlayerStatusObservationContext ) { /* AVPlayerItem "status" property value observer. */
        AVPlayerItemStatus status = [ change[NSKeyValueChangeNewKey] integerValue ];
        switch ( status ) {
                /* Indicates that the status of the player is not yet known because
                 it has not tried to load new media resources for playback */
            case AVPlayerItemStatusUnknown:
                break;
            case AVPlayerItemStatusReadyToPlay:
                /* Once the AVPlayerItem becomes ready to play, i.e.
                 [playerItem status] == AVPlayerItemStatusReadyToPlay,
                 we can start playback using the associated player
                 object. */
                [self startNativePlayerPlayback];
                [self sendPlaybackStart];
                break;
            case AVPlayerItemStatusFailed:
                //                    AVPlayerItem *playerItem = (AVPlayerItem *)object;
                [self cannotPlay];
                break;
        }
    }
    else {
        [ super observeValueForKeyPath: keyPath ofObject: object change: change context: context];
    }
}

#pragma mark ad tag formatting
- ( NSString * )  formattedTagsUrlStringForString:(NSString *)tokenString {
    
    NSString *mediaId = self.entry ? self.entry.id : self.channel ? self.channel.mongoId : @"";
    NSString *mediaName = self.entry ? self.entry.name : self.channel ? self.channel.name : @"";
    mediaName = mediaName ?: @"";
    NSString *mediaDesc = self.entry && self.entry.description ? self.entry.description : self.channel && self.channel.channelDescription ? self.channel.channelDescription : @"";
    mediaDesc = mediaDesc ?: @"";
    NSString *appName = [ NSString stringWithUTF8String: getprogname() ];//[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    
    NSString *appBundle = [ [NSBundle mainBundle] bundleIdentifier ];
    NSString *appVersion = [ [NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString" ];
    NSLocale *currentLocale = [NSLocale currentLocale];
    NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    NSString *currentTimeZone = [ [NSTimeZone localTimeZone] abbreviation ];
    NSArray < NSString * > *preferredLanguages = [NSLocale preferredLanguages];
    NSString *uuid = [ [NSUUID UUID] UUIDString ];
    NSString *ipAddress = [ReachabilityHelper getIPAddress];
    
    if ( !appName ) {
        NSArray <NSString *> *appBundleComponents = [appBundle componentsSeparatedByString:@"."];
        appName = [appBundleComponents lastObject];
    }
    NSString *trimmedTokenString = [tokenString stringByReplacingOccurrencesOfString:@" " withString:@""];
    int playerWidth = 1920;
    int playerHeight = 1080;
    NSDictionary *adTagProperties  = [NSDictionary new];
    
    // FIXED: Only trim mediaUrl for channel data not for adtags
//    NSString *playUrlTrimmed = _playUrl;
//    if (![tokenString isEqualToString:[[PVPConfigHelper sharedInstance] adTagUrl]]) {
//        playUrlTrimmed = _playUrl ? [trimmedTokenString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet] ] : @"";
//        NSArray *splittedUrl = [playUrlTrimmed componentsSeparatedByString:@"?"];
//        if ([splittedUrl count] >= 1) {
//            playUrlTrimmed = splittedUrl[0];
//        }
//    }
    NSString *hasUserAcceptedAdTracking = [[NSUserDefaults standardUserDefaults] boolForKey: IDFA_SYSTEM_REQUEST] ? @"1" :  @"0";
    NSString *isCoppaCompliant = [[PVPConfigHelper sharedInstance] googleMobileAdsTagForChildDirectedTreatment] ? @"1" : @"0";
    NSString *genre = @"";
    
    if (self.entry != nil) {
        if (self.entry.info.allProperties != nil) {
            genre = self.entry.info.allProperties[@"genre"];
        }
    }
    
    if ([genre isEqualToString:@""] || genre == nil) {
        genre = @"entertainment";
    }
    
    if ([trimmedTokenString containsString: @"[player.PLAYER_WIDTH]"]) {
        adTagProperties = @{
            @"[player.PLAYER_WIDTH]": [NSString stringWithFormat:@"%d", playerWidth],
            @"[player.PLAYER_HEIGHT]": [NSString stringWithFormat:@"%d", playerHeight],
            @"[player.MEDIA_ID]": mediaId,
            @"[player.MEDIA_TITLE]": [mediaName stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet] ],
            @"[player.MEDIA_DESCRIPTION]": [mediaDesc stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet] ],
            @"[player.MEDIA_URL]": _playUrl ? [_playUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet] ] : @"",
            @"[player.DEVICE_IDENTIFIER]": [[[PVPConfigHelper sharedInstance] userIDFA] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet] ],
            @"[player.UUID]": uuid,
            @"[player.DEVICE_IP_ADDRESS]": ipAddress,
            @"[player.DEVICE_USER_AGENT]": [AppUtils getDeviceUserAgent], //TODO: check this
            @"[player.DEVICE_PLATFORM]": @"iOS",
            @"[player.APPLICATION_BUNDLE]": appBundle,
            @"[player.APPLICATION_ID]" : appBundle,
            @"[player.APPLICATION_NAME]": [appName stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet] ],
            @"[player.APPLICATION_VERSION]": appVersion,
            @"[player.DEVICE_MOBILE_COUNTRY_CODE]": countryCode ?: @"",
            @"[player.DEVICE_MOBILE_NETWORK_CODE]": @"",
            @"[player.DEVICE_TIME_ZONE]": currentTimeZone,
            @"[player.DEVICE_LOCALE]": [currentLocale localeIdentifier],
            @"[player.DEVICE_LANGUAGE]": [preferredLanguages objectAtIndex:0],
            @"[player.DEVICE_DO_NOT_TRACK_FLAG]": @"false",
            @"[player.TIMESTAMP]" : [@([[NSDate date] timeIntervalSince1970] * 1000) stringValue],
            @"player.CACHEBUSTER" : [@([[NSDate date] timeIntervalSince1970] * 1000) stringValue],
            @"[player.APP_STORE_URL]" : [PVPConfigHelper getLocalConfiguration:@"storeLink"] ?: @"",
            @"[player.DEVICE_MODEL]" : [AppUtils getDeviceModel],
            @"[player.LAT]" : [AppUtils getDefaultValueFofKey:@"latitude"] ?: @"",
            @"[player.LON]" : [AppUtils getDefaultValueFofKey:@"longitude"] ?: @"",
            @"[player.DNT]" : hasUserAcceptedAdTracking,
            @"[player.COPPA]" : isCoppaCompliant,
            @"[player.GENRE]": genre
        };
    } else {
        adTagProperties = @{
            @"[PLAYER_WIDTH]": [NSString stringWithFormat:@"%d", playerWidth],
            @"[PLAYER_HEIGHT]": [NSString stringWithFormat:@"%d", playerHeight],
            @"[MEDIA_ID]": mediaId,
            @"[MEDIA_TITLE]": [mediaName stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet] ],
            @"[MEDIA_DESCRIPTION]": [mediaDesc stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet] ],
            @"[MEDIA_URL]": _playUrl ? [_playUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet] ] : @"",
            @"[DEVICE_IDENTIFIER]": [[[PVPConfigHelper sharedInstance] userIDFA] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet] ],
            @"[UUID]": uuid,
            @"[DEVICE_IP_ADDRESS]": ipAddress,
            @"[DEVICE_USER_AGENT]": [AppUtils getDeviceUserAgent], //TODO: check this
            @"[DEVICE_PLATFORM]": @"iOS",
            @"[APPLICATION_BUNDLE]": appBundle,
            @"[APPLICATION_ID]" : appBundle,
            @"[APPLICATION_NAME]": [appName stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet] ],
            @"[APPLICATION_VERSION]": appVersion,
            @"[DEVICE_MOBILE_COUNTRY_CODE]": countryCode ?: @"",
            @"[DEVICE_MOBILE_NETWORK_CODE]": @"",
            @"[DEVICE_TIME_ZONE]": currentTimeZone,
            @"[DEVICE_LOCALE]": [currentLocale localeIdentifier],
            @"[DEVICE_LANGUAGE]": [preferredLanguages objectAtIndex:0],
            @"[DEVICE_DO_NOT_TRACK_FLAG]": @"false",
            @"[TIMESTAMP]" : [@([[NSDate date] timeIntervalSince1970] * 1000) stringValue],
            @"CACHEBUSTER" : [@([[NSDate date] timeIntervalSince1970] * 1000) stringValue],
            @"[APP_STORE_URL]" : [PVPConfigHelper getLocalConfiguration:@"storeLink"] ?: @"",
            @"[DEVICE_MODEL]" : [AppUtils getDeviceModel],
            @"[LAT]" : [AppUtils getDefaultValueFofKey:@"latitude"] ?: @"",
            @"[LON]" : [AppUtils getDefaultValueFofKey:@"longitude"] ?: @"",
            @"[DNT]" : hasUserAcceptedAdTracking,
            @"[COPPA]" : isCoppaCompliant,
            @"[GENRE]": genre
        };
    }
    
   
    
    if ( tokenString && tokenString.length > 0 ) {
        for ( NSString *key in [adTagProperties allKeys] ) {
            trimmedTokenString = [trimmedTokenString stringByReplacingOccurrencesOfString:key withString:adTagProperties[key]];
        }
    }
    NSString *newURL = [trimmedTokenString stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"#########################");
    NSLog(@"%@",newURL);
    NSLog(@"#########################");
    return newURL;
}

#pragma mark Google IMA SDK
- ( void ) contentDidFinishPlaying: ( NSNotification * ) notification {
    // Make sure we don't call contentComplete as a result of an ad completing.
    if ( notification.object == self.nativePlayer.currentItem ) {
        [self.adsLoader contentComplete];
    }
}

- ( void ) adsLoader: ( IMAAdsLoader * ) loader adsLoadedWithData: ( IMAAdsLoadedData * ) adsLoadedData {
    // Grab the instance of the IMAAdsManager and set ourselves as the delegate.
    self.adsManager = adsLoadedData.adsManager;
    self.adsManager.delegate = self;
    // Create ads rendering settings to tell the SDK to use the in-app browser.
    IMAAdsRenderingSettings *adsRenderingSettings = [ [IMAAdsRenderingSettings alloc] init ];
    // Initialize the ads manager.
    [self.adsManager initializeWithAdsRenderingSettings: adsRenderingSettings];
}

- ( void ) adsLoader: ( IMAAdsLoader * ) loader failedWithErrorData: ( IMAAdLoadingErrorData * ) adErrorData {
    // Something went wrong loading ads. Log the error and play the content.
    NSLog( @"BambooPlayerView: Error loading ads: %@", adErrorData.adError.message );
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self layoutIfNeeded];
        [self resume];
    });
}

- ( void ) adsManager: ( IMAAdsManager * ) adsManager didReceiveAdEvent: ( IMAAdEvent * ) event {
    // When the SDK notified us that ads have been loaded, play them.
    if ( event.type == kIMAAdEvent_LOADED ) {
        // MARK: PIP check if pip is enabled then only start ads
        if (![self.pipController isPictureInPictureActive]) {
            [adsManager start];
        }
        
        [ progressDialog hideAnimated: true ];
        
        [[self playerOverlayView] fadeOut];
        [self hideThumbnailIfNeeded];
        [self.playPauseToggleButtonLarge fadeOut];
        [ self setControlsRevealed: NO ];
    }
    else if ( event.type == kIMAAdEvent_COMPLETE || event.type == kIMAAdEvent_ALL_ADS_COMPLETED) {
        _adIsFinished = YES;
        [self.playPauseToggleButtonLarge fadeIn];
        [[self playerOverlayView] fadeIn];
    }
    else if (event.type == kIMAAdEvent_TAPPED) {
        [self sendAdClick];
    }
    NSLog(@"BambooPlayerView: ADS MANAGER: Event: %@", event.typeString);
}

- ( void ) adsManager: ( IMAAdsManager * ) adsManager didReceiveAdError: ( IMAAdError * ) error {
    // Something went wrong with the ads manager after ads were loaded. Log the error and play the
    // content.
    NSLog( @"BambooPlayerView: AdsManager error: %@", error.message );
    
    [progressDialog hideAnimated:true];
    [self hideThumbnailIfNeeded];
    _adIsFinished = YES;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self layoutIfNeeded];
        [self resume];
    });
}

- ( void ) adsManagerDidRequestContentPause: ( IMAAdsManager * ) adsManager {
    // The SDK is going to play ads, so pause the content.
    NSLog(@"BambooPlayerView: ADS MANAGER: Content pause");
    [self pause: NO ];
}

- ( void ) adsManagerDidRequestContentResume: ( IMAAdsManager * ) adsManager {
    // The SDK is done playing ads (at least for now), so resume the content.
    NSLog(@"BambooPlayerView: ADS MANAGER: Content resume");
    _adIsFinished = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self layoutIfNeeded];
        [self resume];
    });
}

#pragma mark FairPlay DRM
- ( void ) setupDrm {
    NSURL *url = [NSURL URLWithString:_playUrl];
    NSString *deviceType = [[AppUtils getDefaultValueFofKey:IS_SIMULATOR_KEY] boolValue] ? @"Simulator" : @"Device";
    UIDevice *device = [UIDevice currentDevice];
    NSString *deviceInfo = [NSString stringWithFormat:@"Apple %@ %@ %@ %@", device.model, device.systemName, device.systemVersion, deviceType];
    AVURLAsset *asset  = [AVURLAsset URLAssetWithURL:url
                                             options:@{@"AVURLAssetHTTPHeaderFieldsKey" : @{@"User-Agent": @"iOS",
                                                                                                        @"bamboo-req": deviceInfo}}];
    self.loaderDelegate = [[AssetLoaderDelegate alloc] initWithEntryId:self.entryId];
    [[asset resourceLoader] setDelegate:self.loaderDelegate queue:globalNotificationQueue()];
    
    NSArray *requestedKeys = @[BAMBOO_PLAYER_PLAYABLE_KEY];
    
    // Tells the asset to load the values of any of the specified keys that are not already loaded.
    [asset loadValuesAsynchronouslyForKeys:requestedKeys completionHandler:^{
        // callback when a) error, b) cancel, c) playable property is ready
        // make sure we have the value
        NSError *error = nil;
        switch ( [asset statusOfValueForKey:BAMBOO_PLAYER_PLAYABLE_KEY error:&error] ) {
            case AVKeyValueStatusLoaded:
                if ( [asset isPlayable] ) {
                    dispatch_async( dispatch_get_main_queue(), ^{
                        [self initNativePlayerWithAsset:asset];
                    });
                }
                else {
                    NSLog(@"BambooPlayerView: Not playable.");
                }
                break;
                
            case AVKeyValueStatusFailed:
                NSLog(@"BambooPlayerView: No playable status.");
                [self cannotPlay];
                break;
            case AVKeyValueStatusCancelled:
                // If you are also implementing -[AVAsset cancelLoading], add your code here to bail out properly in the case of cancellation.
                NSLog(@"BambooPlayerView: Loading canceled.");
                [self cannotPlay];
                break;
            default:
                // something went wrong; depending on what it was, we may want to dispatch a
                // block to the main thread to report the error
                NSLog(@"BambooPlayerView: Loading error for weird reasons.");
                [self cannotPlay];
                break;
        }
    }];
}

#pragma mark player state
- ( void ) setState: ( BambooPlayerState ) state {
    [self logVideoAnalyticsEventForState:state];
    
    if ( _state != state ) {
        _state = state;
        if ( self.delegate && [self.delegate respondsToSelector:@selector( bambooPlayerStateDidChange: ) ] ) {
            [ self.delegate bambooPlayerStateDidChange: state ];
        }
    }
}

- (void)logVideoAnalyticsEventForState:(BambooPlayerState)state {
    
    switch (state) {
        case BambooPlayerStateWillNotPlay:
            [[PVPPlayerAnalyticsManager sharedInstance] logEvent:PVPPlayerErrorEvent];
            break;
            
        case BambooPlayerStateReady:
            [[PVPPlayerAnalyticsManager sharedInstance] logEvent:PVPPlayerStartEvent];
            break;
            
        case BambooPlayerStatePaused:
            [[PVPPlayerAnalyticsManager sharedInstance] logEvent:PVPPlayerPauseEvent];
            break;
            
        case BambooPlayerStatePlaying: {
            if (_state == BambooPlayerStatePaused) {
                [[PVPPlayerAnalyticsManager sharedInstance] logEvent:PVPPlayerResumeEvent];
            } else {
                [[PVPPlayerAnalyticsManager sharedInstance] logEvent:PVPPlayerPlaysEvent];
            }
            
            break;
        }
            
        default:
            break;
    }
}


#pragma mark entry retrieval
- ( void ) entryRequestSuccessWithEntry: ( PVPEntry * ) entry {
    self.galleryType = [entry isKindOfClass: [PVPLiveEntry class] ] ? BambooGalleryTypeLive : BambooGalleryTypeVOD;
    [ self playEntry: entry withType: self.galleryType ];
}

#pragma mark channel retrieval
- ( void ) channelRequestSuccessWithChannel: ( PVPChannel * ) channel {
    self.galleryType = BambooGalleryTypeChannels;
    [ self playChannel: channel ];
}

#pragma mark config retrieval
- ( void ) initialConfigRequestSuccess {
    [self initPlayer];
}

#pragma isLive callback
- ( void ) liveEntryIsLiveSuccessWithArray: ( NSArray * ) resultArray {
    
    if ( self.entry &&
        ( self.entry.mediaType == [KalturaMediaType LIVE_STREAM_FLASH] || [ self.entry isKindOfClass: [PVPLiveEntry class] ] ) ) {
        
        for ( PVPResult *result in resultArray ) {
            if ( [ self.entry.id isEqualToString: result.id ] ) {
                if ( [result.result boolValue] ) {
                    //If not (catchup is enabled and playing with offest)
                    if ( !( [ [PVPConfigHelper sharedInstance] catchupEnabled ] ) || ( [ [PVPConfigHelper sharedInstance] catchupEnabled ] && self.currentOffset == 0 ) ) {
                        [ self.liveIndicatorLabel setText: [@"Live" localizedString]];
                        [ self.liveIndicatorIcon setImage: [UIImage imageNamed:@"liveIcon"] ];
                        self.liveIndicatorIcon.hidden = NO;
                    }
                }
                else {
                    //If not (catchup is enabled and playing with offest)
                    if ( !( [ [PVPConfigHelper sharedInstance] catchupEnabled ] ) || ( [ [PVPConfigHelper sharedInstance] catchupEnabled ] && self.currentOffset == 0 ) ) {
                        [ self.liveIndicatorLabel setText: [@"Offline" localizedString]];
                        [ self.liveIndicatorIcon setImage: [UIImage imageNamed: @"offlineIcon"] ];
                    }
                }
                
                NSDate *now = [NSDate date];
                NSDate *entryEndDate = [NSDate dateWithTimeIntervalSince1970: self.entry.endDate];
                if ( self.entry.endDate != 0 && [entryEndDate timeIntervalSince1970] < [now timeIntervalSince1970WithServerOffset] ) {
                    [self streamEnded];
                }
                
                break;
            }
        }
    }
}

#pragma mark timers
- ( void ) updateCurrentlyWatching: ( NSTimer * ) timer {
    if ( self.entry
        && self.entry.mediaType == [KalturaMediaType VIDEO]
        && [ [PVPClient sharedInstance] isLoggedIn]
        && [[PVPConfigHelper sharedInstance] isHistoryEnabled]
        && [self isPlaying]
        && self.playbackSeekSlider.value >= self.playbackSeekSlider.maximumValue * 0.2
        )
    {
        PVPWatchHistoryItem *watchItem = [ [PVPWatchHistoryItem alloc] init ];
        watchItem.time = self.playbackSeekSlider.value;
        watchItem.entry = self.entry != nil ? self.entry : self.channel.entry;
        [ self.userModel updateUserWatching: watchItem ];
    }
}

- ( void ) updateIsLive: ( NSTimer * ) timer {
    if ( self.entry &&
        ( self.entry.mediaType == [KalturaMediaType LIVE_STREAM_FLASH] || [self.entry isKindOfClass:[PVPLiveEntry class] ] ) )
    {
        [ self.liveEntryModel checkIfLive: self.entry.id ];
    }
}

#pragma mark seek slider
- ( void ) setupSeekSlider {
    self.playbackSeekSlider.dataSource = self;
}

- ( void ) updateProgressBar: ( CGFloat ) time {
    if (
        time >= 0 &&
        time <= self.playbackSeekSlider.maximumValue + 1 &&
        !self.sliderBeingDragged &&
        self.state != BambooPlayerStateSeeking
        )
    {
        if ( self.state != BambooPlayerStatePaused && self.state != BambooPlayerStatePlaying) {
            [self setState: BambooPlayerStatePlaying];
        }
        self.playbackSeekSlider.value = time;
        self.timeElapsedLabel.text = [self slider: self.playbackSeekSlider stringForValue: time ];
        self.timeLeftLabel.text = [self slider: self.playbackSeekSlider stringForValue: self.playbackSeekSlider.maximumValue - time ];
    }
}

- ( NSString * ) slider: ( ASValueTrackingSlider * ) slider stringForValue: ( float ) value {
    int intValue = value;
    int hours = intValue / 3600;
    int minutes = intValue % 3600 / 60;
    int seconds = intValue % 60;
    
    if ( hours > 0 ) {
        return [ NSString stringWithFormat: @"%d:%02d:%02d", hours, minutes, seconds ];
    }
    else {
        return [ NSString stringWithFormat: @"%d:%02d", minutes, seconds ];
    }
}

- ( IBAction ) seekSliderDragStart: ( ASValueTrackingSlider * ) sender {
    [self setControlsRevealed:YES];
    [self destroyControlRevealTimer];
    
    [ self setState:BambooPlayerStateSeeking ];
    self.sliderBeingDragged = YES;
}

- ( IBAction ) seekSliderValueUpdated: ( ASValueTrackingSlider * ) sender {
    self.timeElapsedLabel.text = [ self slider: self.playbackSeekSlider stringForValue: sender.value ];
    self.timeLeftLabel.text = [ self slider: self.playbackSeekSlider stringForValue: self.playbackSeekSlider.maximumValue - sender.value ];
}

- ( IBAction ) seekSliderValueChanged: ( ASValueTrackingSlider * ) sender {
    [self setControlsRevealed:YES];
    CMTime time = CMTimeMake(sender.value, 1);
    
    if ( self.useNativePlayer && self.nativePlayer ) {
        [ self.nativePlayer seekToTime: time ];
        [ self sendAnalyticsEventWithType: KalturaAnalyticsEventTypeSeek withCurrentTime: CMTimeGetSeconds( self.nativePlayer.currentTime ) ];
        [ self sendGoogleAnalyticsEventWithAction: @"Seek" withCurrentTime: CMTimeGetSeconds( self.nativePlayer.currentTime ) ];
    }
    //    else if (self.kPlayer) {
    //        [self.kPlayer seekTo:time];
    //    }
    
    self.timeElapsedLabel.text = [ self slider: self.playbackSeekSlider stringForValue: sender.value ];
    self.timeLeftLabel.text = [ self slider: self.playbackSeekSlider stringForValue: self.playbackSeekSlider.maximumValue - sender.value ];
    
    dispatch_after( dispatch_time( DISPATCH_TIME_NOW, ( int64_t )( 0.3 * NSEC_PER_SEC ) ), dispatch_get_main_queue(), ^{
        self.sliderBeingDragged = NO;
        [ self setState: [self isPlaying] ? BambooPlayerStatePlaying : BambooPlayerStatePaused ];
    });
    
    [self updateCurrentlyWatching: self.updateWatchingTimer];
    [self sendAnalyticsEventWithType: KalturaAnalyticsEventTypeSeek withCurrentTime: sender.value];
    [self sendGoogleAnalyticsEventWithAction: @"Seek" withCurrentTime: sender.value];
}
/// This function is for setting up caption
- (void) setUpCaption {
    bool enableSubtitle = false;
    if (self.nativePlayer.currentItem != nil) {
        AVMediaSelectionGroup *subtitleSelectionGroup = [self.nativePlayer.currentItem.asset mediaSelectionGroupForMediaCharacteristic:AVMediaCharacteristicLegible];
        for (AVMediaSelectionOption *item in subtitleSelectionGroup.options) {
            if (![item.displayName isEqualToString: @"CC"]) {
                enableSubtitle = true;
            }
        }
    }
    [self.captionButton setHidden: !enableSubtitle];
}
#pragma mark player controls
- ( void ) setPlayerAssetsHidden {
    [ progressDialog hideAnimated: true ];
    
    self.playPauseToggleButtonLarge.hidden = YES;
    
    self.playerControlsView.hidden = YES;
    self.livePlayerControlsView.hidden = YES;
    
    self.playerLogo.hidden = YES;
    self.livePlayerLogo.hidden = YES;
    
    self.playbackSeekSlider.hidden = YES;
    self.timeLeftLabel.hidden = YES;
    self.timeElapsedLabel.hidden = YES;
}

- ( void ) setControlsRevealed: ( BOOL ) revealed {
    
    _controlsRevealed = revealed;
    
    [self destroyControlRevealTimer];
    
    if ( revealed ) {
        if ( self.entry.mediaType == [KalturaMediaType LIVE_STREAM_FLASH] || self.channel ) {
            [self.livePlayerControlsView fadeIn];
        }
        else {
            [ self.playerControlsView fadeIn ];
        }
        [self initControlRevealTimer];
    }
    else {
        [ self.playPauseToggleButtonLarge setImage: nil forState: UIControlStateNormal ];
        [ self.playPauseToggleButtonLarge setImage: nil forState: UIControlStateSelected ];
        
        [ self.playerControlsView fadeOut ];
        [ self.livePlayerControlsView fadeOut ];
    }
}

- ( BOOL ) isControlsRevealed {
    return _controlsRevealed;
}

- ( void ) hidePlayerControls: ( NSTimer * ) timer {
    [ self setControlsRevealed: NO ];
}

- ( void ) checkPlayerState {
    if ( self.nativePlayer && ![self isPlaying] ) {
        //        self.playPauseToggleButtonLarge.hidden = NO;
        //        self.playerControlsView.hidden = YES;
        //        self.livePlayerControlsView.hidden = YES;
    }
}

#pragma mark player layer frame
- ( void ) updateNativePlayerLayerFrame {
    
    if ( UIDeviceOrientationIsLandscape( [UIDevice currentDevice].orientation ) ) {
        
        [ [UIApplication sharedApplication] setStatusBarHidden: YES withAnimation: UIStatusBarAnimationFade ];
        
        //        _nativePlayerLayer.frame = CGRectMake( 0, 0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width );
        [self.playerWindowArrangerButton setSelected:true];
        //_nativePlayerLayer.frame = [UIScreen mainScreen].bounds;
    }
    else if ( UIDeviceOrientationIsPortrait( [UIDevice currentDevice].orientation ) ) {
        
        [ [UIApplication sharedApplication] setStatusBarHidden: NO withAnimation: UIStatusBarAnimationFade ];
        
        if (!self.shouldRotate) {
            //_nativePlayerLayer.frame = CGRectMake( 0, 0, self.bounds.size.width, self.bounds.size.height);
            [self.playerWindowArrangerButton setSelected:true];
        } else {
            //_nativePlayerLayer.frame = CGRectMake( 0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.width / 16 * 9 );
            [self.playerWindowArrangerButton setSelected:false];
        }
        
        //        _nativePlayerLayer.frame = self.superview.bounds;
    }
    
}

#pragma mark - orientation change
- ( void ) orientationChanged: ( NSNotification * ) notification {
    UIDevice *dev = notification.object;
    NSLog( @"BambooPlayerView: Orientation changed %ld", ( long )dev.orientation );
    if (_shouldRotate) {
        [self redrawPlayerWithOrientation: dev.orientation];
    }
}

- ( void ) redrawPlayerWithOrientation: ( UIDeviceOrientation ) orientation {
    
    BOOL isOrientationLandscape = ( orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight );
    BOOL isOrientationPortrait = ( orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown );
    
    CGRect newFrame = self.frame;
    
    if ( isOrientationLandscape ) {
        NSLog( @"BambooPlayerView: landscape" );
        [ [UIApplication sharedApplication] setStatusBarHidden: YES withAnimation: UIStatusBarAnimationFade ];
        //_nativePlayerLayer.frame = CGRectMake( 0, 0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width );
    }
    else if ( isOrientationPortrait ) {
        NSLog( @"BambooPlayerView: portrait" );
        [ [UIApplication sharedApplication] setStatusBarHidden: NO withAnimation: UIStatusBarAnimationFade ];
        //newFrame = self.superview.bounds;
    }
    else {
        NSLog( @"BambooPlayerView: Some other orientation" );
    }
    
    self.frame = newFrame;
    
    if ( self.nativePlayer ) {
        _nativePlayerLayer.frame = newFrame;
        [self updateNativePlayerLayerFrame];
        
        if ( isOrientationLandscape ) {
            _nativePlayerLayer.videoGravity = [ [PVPConfigHelper sharedInstance] playerVideoGravity ];
            
            [ self sendAnalyticsEventWithType: KalturaAnalyticsEventTypeOpenFullScreen withCurrentTime: CMTimeGetSeconds( self.nativePlayer.currentTime ) ];
            [ self sendGoogleAnalyticsEventWithAction: @"OpenFullscreen" withCurrentTime: CMTimeGetSeconds( self.nativePlayer.currentTime ) ];
        }
        else if ( isOrientationPortrait ) {
            _nativePlayerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
            
            [ self sendAnalyticsEventWithType: KalturaAnalyticsEventTypeCloseFullScreen withCurrentTime: CMTimeGetSeconds( self.nativePlayer.currentTime ) ];
            
            [ self sendGoogleAnalyticsEventWithAction: @"CloseFullscreen" withCurrentTime: CMTimeGetSeconds( self.nativePlayer.currentTime ) ];
        }
    }
    //    else if (self.kPlayer) {
    //        self.kPlayer.view.frame = CGRectMake(0, 0, newFrame.size.width, newFrame.size.height);
    //    }
    
    [self layoutIfNeeded];
}

- ( NSString * ) timerLabel: ( MZTimerLabel * ) timerLabel customTextToDisplayAtTime: ( NSTimeInterval ) time {
    int d = time / 86400;
    int h = ((int) time % 86400) / 3600;
    int m = (int) (time / 60) % 60;
    int s = (int) time % 60;
    
    return [NSString stringWithFormat:@"%02d:%02d:%02d:%02d", d, h, m, s];
}

- ( void ) sendAnalyticsEventWithType: ( KalturaAnalyticsEventType ) eventType withCurrentTime: ( int ) currentTime {
    switch ( self.galleryType ) {
        case BambooGalleryTypeChannels:
            //handle channels?
            break;
        case BambooGalleryTypeLive:
            //handle live?
            break;
        case BambooGalleryTypeOffline:
            //handle offline?
            break;
        default:
            [ [KalturaAnalyticsHelper sharedInstance] sendEventWithType: eventType withEntry: self.entry currentPoint: currentTime ];
            break;
    }
}

- (void) sendGoogleAnalyticsEventWithAction:(NSString *)action withCurrentTime:(int)currentTime {
    
    [FIRAnalytics logEventWithName:@"bamboo_player_events"
                        parameters:@{
        kFIRParameterItemID:[self getCurrentItemID] ,
        kFIRParameterItemName:[NSString stringWithFormat:@"video_time_%d", currentTime],
        @"action":action
    }];
}

- (NSString *)getCurrentItemID {
    if (self.entry != nil) {
        return [self.entry eventIDString];
    } else if (self.channel != nil) {
        return [self.channel eventIDString];
    } else {
        return @"no_entry_id";
    }
}

#pragma mark app state notifications

- ( void ) applicationDidEnterBackground:(NSNotification *) note {
    if (![[PVPConfigHelper sharedInstance] allowBackgroundPlayback]) {
        _nativePlayerLayer.player = nil;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleAudioSessionInterruption:)
                                                 name:AVAudioSessionInterruptionNotification object:[AVAudioSession sharedInstance]];
}

- ( void ) applicationWillEnterForeground:(NSNotification *) note {
    _nativePlayerLayer.player = _nativePlayer;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVAudioSessionInterruptionNotification object:nil];
    
    if ( self.adsManager ) {
        [self.adsManager resume];
    }
}

- (void)appWillResignActive {
    // MARK: PIP
    if (![[PVPConfigHelper sharedInstance] allowBackgroundPlayback]) {
        [self pause:self.galleryType != BambooGalleryTypeChannels && self.galleryType != BambooGalleryTypeLive];
    } else {
        [self.pipController startPictureInPicture];
    }
    
}

- (void) appDidBecomeActive {
    
    if (![[PVPConfigHelper sharedInstance] allowBackgroundPlayback]) {
        if ( (self.galleryType == BambooGalleryTypeChannels || self.galleryType == BambooGalleryTypeLive) && ![self isPlaying] ) {
            [self resume];
        }
    } else {
        [self.pipController stopPictureInPicture];
    }
    
    if ( self.adsManager ) {
        [self.adsManager resume];
    }
}

- ( void ) appWillTerminate: ( NSNotification * ) note {
    [self destroy];
}

static dispatch_queue_t	globalNotificationQueue ( void ) {
    static dispatch_queue_t globalQueue = 0;
    static dispatch_once_t getQueueOnce = 0;
    dispatch_once(&getQueueOnce, ^{
        globalQueue = dispatch_queue_create( "tester notify queue", NULL );
    });
    return globalQueue;
}

- ( void ) showLoadingHUD: ( NSString * ) title {
    
    [ progressDialog hideAnimated: true ];
    progressDialog = [ MBProgressHUD showHUDAddedTo: self.videoView animated: YES ];
    
    progressDialog.label.text = title;
    progressDialog.bezelView.color = [UIColor clearColor];
    UIImageView *imageViewAnimatedGif = [ [UIImageView alloc] init ];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource: @"custom_loader" ofType: @"gif"];
    if (filePath != nil) {
        NSData *gifData = [NSData dataWithContentsOfFile: filePath];
        imageViewAnimatedGif.contentMode = UIViewContentModeScaleAspectFit;
        imageViewAnimatedGif.image = [UIImage sd_imageWithGIFData:gifData];
        progressDialog.customView = imageViewAnimatedGif;
    } else {
        BambooActivityAnimationView *view = [[BambooActivityAnimationView alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
        view.spinDuration = 2.0;
        view.lineWidth = 5.0;
        view.colors = @[[[PVPConfigHelper sharedInstance] spinnerColor]];
        [view startAnimating];
        progressDialog.customView = view;
    }
    
    progressDialog.mode = MBProgressHUDModeCustomView;
    progressDialog.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    progressDialog.contentColor = [UIColor clearColor];
    progressDialog.backgroundColor = [UIColor clearColor];
    [ progressDialog showAnimated: YES ];
}

- ( void ) destroy {
    [self sendPlaybackEnd];
    @try {
        [ [NSNotificationCenter defaultCenter] removeObserver: self name: UIApplicationWillTerminateNotification object: nil ];
    } @catch ( NSException *exception ) {
        NSLog( @"BambooPlayerView: FAIL - native player observer for 'UIApplicationWillTerminateNotification' removal. Error: %@", exception );
    }
    
    @try {
        [ [NSNotificationCenter defaultCenter] removeObserver: self name: UIDeviceOrientationDidChangeNotification object: nil ];
    } @catch ( NSException *exception ) {
        NSLog( @"BambooPlayerView: FAIL - native player observer for 'UIDeviceOrientationDidChangeNotification' removal. Error: %@", exception );
    }
    
    
    @try {
        [ [NSNotificationCenter defaultCenter] removeObserver: self name: AVPlayerItemDidPlayToEndTimeNotification object: nil ];
    } @catch ( NSException *exception ) {
        NSLog( @"BambooPlayerView: FAIL - native player observer for 'AVPlayerItemDidPlayToEndTimeNotification' removal. Error: %@", exception );
    }
    @try {
        [ [NSNotificationCenter defaultCenter] removeObserver: self];
    } @catch ( NSException *exception ) {
        NSLog( @"BambooPlayerView: FAIL - native player observer removal. Error: %@", exception );
    }
    
    if ( self.nativePlayer ) {
        [self.nativePlayer pause];
    }
    
    MPRemoteCommandCenter *remoteCenter = [MPRemoteCommandCenter sharedCommandCenter];
    [remoteCenter.playCommand removeTarget:self];
    [remoteCenter.pauseCommand removeTarget:self];
    [[AVAudioSession sharedInstance] setActive:NO error:nil];
    [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:nil];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    
    if ( _nativePlayerItem ) {
        /* Remove existing player item key value observers. */
        @try {
            [ _nativePlayerItem removeObserver: self forKeyPath: BAMBOO_PLAYER_STATUS_KEY ];
        } @catch ( NSException *exception ) {
            NSLog( @"BambooPlayerView: FAIL - _nativePlayerItem observer for 'BAMBOO_PLAYER_STATUS_KEY' removal. Error: %@", exception );
        } @finally {
            NSLog( @"BambooPlayerView: OK - _nativePlayerItem observer for 'BAMBOO_PLAYER_STATUS_KEY' removal" );
        }
        
        @try {
            [ _nativePlayerItem removeObserver: self forKeyPath: AVPlayerItemDidPlayToEndTimeNotification ];
        } @catch ( NSException *exception ) {
            NSLog( @"BambooPlayerView: FAIL - _nativePlayerItem observer for 'AVPlayerItemDidPlayToEndTimeNotification' removal. Error: %@", exception );
        } @finally {
            NSLog( @"BambooPlayerView: OK - _nativePlayerItem observer for 'AVPlayerItemDidPlayToEndTimeNotification' removal" );
        }
        _nativePlayerItem = nil;
    }
    
    if ( self.nativePlayer ) {
        
        @try {
            [self.nativePlayer removeObserver:self forKeyPath:@"rate"];
        } @catch ( NSException *exception ) {
            NSLog( @"BambooPlayerView: FAIL - native player observer for 'rate' removal. Error: %@", exception );
        } @finally {
            NSLog( @"BambooPlayerView: OK - native player observer for 'rate' removal" );
        }
        
        @try {
            [self.nativePlayer removeObserver:self forKeyPath:@"status"];
        } @catch ( NSException *exception ) {
            NSLog( @"BambooPlayerView: FAIL - native player observer for 'status' removal. Error: %@", exception );
        } @finally {
            NSLog( @"BambooPlayerView: OK - native player observer for 'status' removal" );
        }
        
        @try {
            [self.nativePlayer.currentItem removeObserver:self forKeyPath:@"status"];
        } @catch ( NSException *exception ) {
            NSLog( @"BambooPlayerView: FAIL - self.nativePlayer.currentItem observer for 'status' removal. Error: %@", exception );
        } @finally {
            NSLog( @"BambooPlayerView: OK - self.nativePlayer.currentItem observer for 'status' removal" );
        }
        
        if ( self.playerObserverUpdateVideoProgressStats ) {
            @try {
                [self.nativePlayer removeTimeObserver:self.playerObserverUpdateVideoProgressStats];
            } @catch ( NSException *exception ) {
                NSLog( @"BambooPlayerView: FAIL - native player timeObserver (self.playerObserverUpdateVideoProgressStats) for time interval removal. Error: %@", exception );
            } @finally {
                NSLog( @"BambooPlayerView: OK - native player timeObserver (self.playerObserverUpdateVideoProgressStats) for time interval removal" );
                self.playerObserverUpdateVideoProgressStats = nil;
            }
        }
        
        if ( self.playerObserverUpdateProgressBar ) {
            @try {
                [self.nativePlayer removeTimeObserver:self.playerObserverUpdateProgressBar];
            } @catch ( NSException *exception ) {
                NSLog( @"BambooPlayerView: FAIL - native player timeObserver (self.playerObserverUpdateProgressBar) for time interval removal. Error: %@", exception );
            } @finally {
                NSLog( @"BambooPlayerView: OK - native player timeObserver (self.playerObserverUpdateProgressBar) for time interval removal." );
                self.playerObserverUpdateProgressBar = nil;
            }
        }
        
        if ( self.timeObserverBlockUpdateVideoProgressStats ) {
            self.timeObserverBlockUpdateVideoProgressStats = nil;
        }
        
        if ( self.timeObserverBlockUpdateProgressBar ) {
            self.timeObserverBlockUpdateProgressBar = nil;
        }
        
        //        [self.nativePlayer cancelPendingPrerolls];
        //        [self.nativePlayer replaceCurrentItemWithPlayerItem:nil];
    } // if self.nativePlayer
    
    if ( self.videoView ) {
        for (UIView *view in [self.videoView subviews]) {
            [view removeFromSuperview];
        }
        
        for ( int i = 0; i < self.videoView.layer.sublayers.count; i++ ) {
            CALayer *layer = self.videoView.layer.sublayers[ i ];
            
            @try {
                [layer removeFromSuperlayer];
            } @catch ( NSException *exception ) {
                NSLog( @"BambooPlayerView: FAIL - native player removing layer index: %d. Error: %@", i, exception );
            } @finally {
                NSLog( @"BambooPlayerView: OK - native player removing layer index: %d.", i );
            }
            i--;
        }
    }
    
    if ( self.userModel ) {
        self.userModel.delegate = nil;
        self.userModel = nil;
    }
    if ( self.configModel ) {
        self.configModel.delegate = nil;
        self.configModel = nil;
    }
    if ( self.entryModel ) {
        self.entryModel.delegate = nil;
        self.entryModel = nil;
    }
    if ( self.liveEntryModel ) {
        self.liveEntryModel.delegate = nil;
        self.liveEntryModel = nil;
    }
    if ( self.channelModel ) {
        self.channelModel.delegate = nil;
        self.channelModel = nil;
    }
    
    self.entry = nil;
    self.node = nil;
    self.channel = nil;
    
    if ( self.playbackSeekSlider ) {
        self.playbackSeekSlider.dataSource = nil;
        self.playbackSeekSlider = nil;
    }
    if ( self.adsManager ) {
        self.adsManager.delegate = nil;
        [self.adsManager destroy];
        self.adsManager = nil;
    }
    if ( _adsLoader ) {
        _adsLoader.delegate = nil;
        _adsLoader = nil;
    }
    if ( _contentPlayhead ) {
        _contentPlayhead = nil;
    }
    if ( _loaderDelegate ) {
        _loaderDelegate = nil;
    }
    
    [self destroyControlRevealTimer];
    
    if ( self.updateWatchingTimer ) {
        [self.updateWatchingTimer invalidate];
        self.updateWatchingTimer = nil;
    }
    if ( self.isLiveTimer ) {
        [self.isLiveTimer invalidate];
        self.isLiveTimer = nil;
    }
    if ( self.liveCountdownTimer ) {
        [self.liveCountdownTimer pause];
        [self.liveCountdownTimer removeFromSuperview];
        self.liveCountdownTimer.delegate = nil;
        self.liveCountdownTimer = nil;
    }
    progressDialog = nil;
    self.thumbImage = nil;
    self.preloadedImage = nil;
    
    if ([self.videoView superview] != nil) {
        [self.videoView removeFromSuperview];
    }
    
    self.videoView = nil;
    
    if ([self.playerOverlayView superview] != nil) {
        [self.playerOverlayView removeFromSuperview];
    }
    self.playerOverlayView = nil;
    
    if ([self.playerControlsView superview] != nil) {
        [self.playerControlsView removeFromSuperview];
    }
    
    if ([self.livePlayerControlsView superview] != nil) {
        [self.livePlayerControlsView removeFromSuperview];
    }
    
    self.playerControlsView = nil;
    self.livePlayerControlsView = nil;
    
    self.nativePlayerLayer = nil;
    self.loaderDelegate = nil;
    self.delegate = nil;
    self.nativePlayer = nil;
}

- ( void ) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"%@",self);
    NSLog(@"Deinitialized");
}

#pragma mark - Remote Controls

- (void)updateNowPlayingIfNeeded {
    if ([[PVPConfigHelper sharedInstance] allowBackgroundPlayback]) {
        NSString *title = nil;
        if (self.entry.name.length > 0) {
            title = self.entry.name;
        } else if(self.channel.entry.name.length > 0) {
            title = self.channel.entry.name;
        }
        
        if(title.length > 0) {
            [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:@{MPMediaItemPropertyTitle : title}];
        }
    }
}

- (void)setupRemoteControls
{
    NSError *sessionError = nil;
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( applicationDidEnterBackground: ) name: UIApplicationDidEnterBackgroundNotification object: nil ];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( applicationWillEnterForeground: ) name: UIApplicationWillEnterForegroundNotification object: nil ];
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    if (sessionError == nil) {
        [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    }
    
    MPRemoteCommandCenter *remoteCenter = [MPRemoteCommandCenter sharedCommandCenter];
    [remoteCenter.playCommand setEnabled:YES];
    [remoteCenter.playCommand addTarget:self action:@selector(remoteControlResume)];
    
    [remoteCenter.pauseCommand setEnabled:YES];
    [remoteCenter.pauseCommand addTarget:self action:@selector(remoteControlPause)];
    
}

- (MPRemoteCommandHandlerStatus)remoteControlPause {
    if ( self.adsManager ) {
        [self.adsManager pause];
    }
    [self pause:NO];
    
    return MPRemoteCommandHandlerStatusSuccess;
}

- (MPRemoteCommandHandlerStatus)remoteControlResume {
    if ( self.adsManager && _adIsFinished == NO) {
        [self.adsManager resume];
    } else {
        [self resume];
    }
    
    return MPRemoteCommandHandlerStatusSuccess;
}

- (void)handleAudioSessionInterruption:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];
    if (userInfo[AVAudioSessionInterruptionTypeKey] != nil) {
        AVAudioSessionInterruptionType type = (AVAudioSessionInterruptionType)[(NSNumber *) userInfo[AVAudioSessionInterruptionTypeKey] unsignedIntegerValue];
        switch (type) {
            case AVAudioSessionInterruptionTypeBegan:
            {
                if ( self.adsManager ) {
                    [self.adsManager pause];
                }
                [self pause:NO];
                
                break;
            }
                
            case AVAudioSessionInterruptionTypeEnded:
            {
                NSNumber *result = userInfo[AVAudioSessionInterruptionOptionKey];
                AVAudioSessionInterruptionOptions options = (AVAudioSessionInterruptionOptions)[result unsignedIntegerValue];
                
                if (options == AVAudioSessionInterruptionOptionShouldResume && self.adsManager && !_adIsFinished) {
                    [self.adsManager resume];
                } else if (options == AVAudioSessionInterruptionOptionShouldResume && [[self nativePlayer] timeControlStatus] == AVPlayerTimeControlStatusPaused) {
                    [self resume];
                }
                
                break;
            }
                
            default:
                break;
        }
    }
    
}

- ( void ) pictureInPictureController:(AVPictureInPictureController *)pictureInPictureController restoreUserInterfaceForPictureInPictureStopWithCompletionHandler:(void (^)(BOOL))completionHandler {
    completionHandler(true);
}

-( void) pictureInPictureController:(AVPictureInPictureController *)pictureInPictureController failedToStartPictureInPictureWithError:(NSError *)error {
    NSLog(@"%@", NSStringFromCGRect(_nativePlayerLayer.frame));
    NSLog(@"%@", _nativePlayerLayer);
    NSLog(@"%@", error);
}

#pragma mark - PALNonceLoaderDelegate methods

- (void)nonceLoader:(PALNonceLoader *)nonceLoader
        withRequest:(PALNonceRequest *)request
didLoadNonceManager:(PALNonceManager *)nonceManager {
    NSLog(@"Programmatic access nonce: %@", nonceManager.nonce);
    // Capture the created nonce manager and attach its gesture recognizer to the video view.
    self.nonceManager = nonceManager;
    //[self.videoView addGestureRecognizer:self.nonceManager.gestureRecognizer];
}

- (void)nonceLoader:(PALNonceLoader *)nonceLoader
        withRequest:(PALNonceRequest *)request
   didFailWithError:(NSError *)error {
    NSLog(@"Error generating programmatic access nonce: %@", error);
}

#pragma mark - UI Callback methods
/**
 * Requests a new nonce manager with a request containing arbitrary test values like a (sane) user
 * might supply. Displays the nonce or error on success. This should be called once per stream.
 */
- (void) requestNonceManager {
    PVPPalConfig * config = [[PVPConfigHelper sharedInstance] getPalSDKConfigs];
    PALNonceRequest *request = [[PALNonceRequest alloc] init];
    request.continuousPlayback = PALFlagOff;
    request.descriptionURL = [NSURL URLWithString:@"https://example.com/desc?key=val"];
    request.iconsSupported = YES;
    request.playerType = @"AwesomePlayer";
    request.playerVersion = @"4.2.1";
    request.PPID = @"123987456";
    request.sessionID = @"Sample SID";
    request.videoPlayerHeight = 480;
    request.videoPlayerWidth = 640;
    request.willAdAutoPlay = PALFlagOn;
    request.willAdPlayMuted = PALFlagOff;
    request.OMIDPartnerName = config.omidPartnerName;
    request.OMIDPartnerVersion = config.omidPartnerVersion;
    request.OMIDVersion = config.omidVersion;
    
    if (self.nonceManager) {
        // Detach the old nonce manager's gesture recognizer before destroying it.
        //[self.videoView removeGestureRecognizer:self.nonceManager.gestureRecognizer];
        self.nonceManager = nil;
    }
    [self.nonceLoader loadNonceManagerWithRequest:request];
}

/** Reports the start of playback for the current content session. */
- (void) sendPlaybackStart {
    [self.nonceManager sendPlaybackStart];
}

/** Reports the end of playback for the current content session. */
- (void) sendPlaybackEnd {
    [self.nonceManager sendPlaybackEnd];
}

/** Reports an ad click for the current nonce manager, if not nil. */
- (void) sendAdClick {
    [self.nonceManager sendAdClick];
}
@end
