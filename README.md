# BambooPlayer

[![CI Status](https://img.shields.io/badge/build-stable-brightgreen.svg?style=flat)](https://bitbucket.org/pandaos/bamboo-player-objc)
[![Version](https://img.shields.io/badge/pod-v0.2.2-blue.svg?style=flat)](https://bitbucket.org/pandaos/bamboo-player-objc)
[![License](https://img.shields.io/badge/license-all%20rights%20reserved-lightgrey.svg?style=flat)](https://bitbucket.org/pandaos/bamboo-player-objc)
[![Language](https://img.shields.io/badge/language-objective%20c-lightgrey.svg?style=flat)](https://bitbucket.org/pandaos/bamboo-player-objc)
[![Platform](https://img.shields.io/badge/platform-ios-lightgrey.svg?style=flat)](https://bitbucket.org/pandaos/bamboo-player-objc)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements
The BambooPlayer library has been fully tested on devices running iOS 8.0. The library currently only works on real devices, and does not work on a simulator (will hopefully be fixed soon).

## Installation

BambooPlayer is available through the [Panda-OS Podspec Repository](https://bitbucket.org/pandaos/panda-podspec-repo). To install
it, simply add both the Cocoapods and the Panda-OS podspec repositories to the sources in your Podfile:

````
source 'https://github.com/CocoaPods/Specs.git'
source 'https://bitbucket.org/pandaos/panda-podspec-repo'
````

And then add the following line to your Podfile to include the library:

````
pod "BambooPlayer"
````

And run `pod install` from the terminal, from the main project directory to install the library.

Standard usage of the player would be to add a BambooPlayer view to your view controller via the storyboard, and play media with it:

````
@property (weak, nonatomic) IBOutlet BambooPlayer *player;

[self.player playEntry:<PVPEntry>];
[self.player playEntry:<PVPEntry> withType:BambooGalleryTypeVOD];
[self.player playChannel:<PVPChannel>];
````

## Author

Oren Kosto, , vladimir@panda-os.com

## License

PVPClient is available under copyright (c) 2017 Panda-OS. All rights reserved.
See the LICENSE file for more info.


## Release Notes
* 0.2.2
    * Upgraded GVR SDK to 1.70.0.
* 0.2.1
    * Ad integration for the 360 player.
* 0.2.0
    * Fixed issues with the DRM mechanism.
    * Fixed compile errors on XCode 9.
    * Other bug fixes.
* 0.1.9
    * Bug fixes.
* 0.1.8
    * Adding BambooPlayer to the user agent for analytics purposes.
* 0.1.7
    * Google Analytics tracking for the 360 player.
* 0.1.6
    * Google Analytics tracking.
    * Fairplay DRM.
    * Custom player logo support.
    * Location-based blocking.
    * Beacon support.
