//
//  BambooViewController.m
//  BambooPlayer
//
//  Created by orenk86 on 11/28/2016.
//  Copyright (c) 2016 orenk86. All rights reserved.
//

#import "BambooViewController.h"

@interface BambooViewController ()
@property (weak, nonatomic) IBOutlet BambooPlayer *playerContainer;

@end

@implementation BambooViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.entryModel = [[PVPEntryModel alloc] init];
    self.entryModel.delegate = self;
    self.configModel = [[PVPConfigModel alloc] init];
    self.configModel.delegate = self;
    self.channelModel = [[PVPChannelModel alloc] init];
    self.channelModel.delegate = self;
    [self.configModel getServerConfig];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)configRequestSuccess {
    [self.entryModel list];
//    [self.channelModel list];
//    [self.playerContainer play360VideoUrl:@"https://vp.nyt.com/video/360/hls/video.m3u8"];
}

-(void)entryRequestSuccessWithEntriesArray:(NSArray *)entriesArray {
//    [self.playerContainer loadPlayerIntoView:self.playerContainer withDelegate:self];
    PVPEntry *entry = entriesArray[1];
    [self.playerContainer playEntryWithId:entry.id];
}

-(void)channelRequestSuccessWithChannels:(NSArray<PVPChannel *> *)channelsArray {
//    [self.playerContainer loadPlayerIntoView:self.playerContainer withDelegate:self];
    PVPChannel *channel = channelsArray[0];
    [self.playerContainer playChannelWithId:channel.mongoId];
}

@end
