//
//  BambooViewController.h
//  BambooPlayer
//
//  Created by orenk86 on 11/28/2016.
//  Copyright (c) 2016 orenk86. All rights reserved.
//

@import UIKit;

#import <BambooPlayer/BambooPlayer.h>

@interface BambooViewController : UIViewController <PVPEntryModelDelegate, PVPConfigModelDelegate, PVPChannelModelDelegate, BambooPlayerDelegate>

@property(nonatomic, strong) PVPConfigModel *configModel;
@property(nonatomic, strong) PVPEntryModel *entryModel;
@property(nonatomic, strong) PVPChannelModel *channelModel;

@end
