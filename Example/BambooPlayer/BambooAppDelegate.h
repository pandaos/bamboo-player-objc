//
//  BambooAppDelegate.h
//  BambooPlayer
//
//  Created by orenk86 on 11/28/2016.
//  Copyright (c) 2016 orenk86. All rights reserved.
//

@import UIKit;

@interface BambooAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
