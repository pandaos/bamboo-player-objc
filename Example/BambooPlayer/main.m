//
//  main.m
//  BambooPlayer
//
//  Created by orenk86 on 11/28/2016.
//  Copyright (c) 2016 orenk86. All rights reserved.
//

@import UIKit;
#import "BambooAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BambooAppDelegate class]));
    }
}
